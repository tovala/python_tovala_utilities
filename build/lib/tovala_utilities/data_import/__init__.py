from __future__ import division
import csv
import numpy as np
import math
from datetime import datetime
from .. import math_utility as mu
from .. import time_manipulation as tman

VERBOSITY = 2

#General purpose CSV importing function. Attempts to dump headers. Trust but verify.
def importCSV(filename,headers = 0):

	_time = []
	_data = []

	x = -1

	with open(filename, "rt") as csvfile: #with open(filename, "rt", encoding='utf-8') as csvfile:
		data = csv.reader(csvfile, delimiter=',', quotechar='|');
		for row in data:

			if (x == -1):

				if(headers==0):
					_time.append(row[0])
					#print("Debug Fail")

				#Do Things for Header Row
				for i in range(1,len(row)):
					_data.append([format("Column %d"%i)])

					if(headers==0):
						_data[i-1].append(row[i])
						#print("Debug Fail")

				x = 0

			else:
				_time.append(row[0])

				for i in range(1,len(row)):
					
					_data[i-1].append(row[i])	

	_time = mu.prepArray(_time)
	#print(len(_data[0]))
	_dataNP = np.zeros((len(_data[0])-1,len(_data)))
	for i in range(len(_data)):
		_dataNP[:,i] = np.asarray(_data[i][1:len(_data[0])])

	return [_time,_dataNP]

#General purpose CSV importing function. Attempts to dump headers, but does no processing on data block. Use for pulling in large blocks of unknown/unstructured data.
#Trust but verify.
def importArbitraryCSV(filename,headers = 0):

	_index = []
	_data = []

	x = 1

	with open(filename,'rt') as csvfile:
		data = csv.reader(csvfile, delimiter=',', quotechar='"');
		for row in data:

			#print(x)

			if (x < headers):

				headerContent = row[0].split(": ")

				if(headerContent[0] == "Scan Rate"):
					print("File Properly Parsed")
					scanrate = float(headerContent[1])

				x = x + 1

			elif (x == headers):
				#Do Things for Header Row
				for i in range(1,len(row)):
					_data.append([format("Column %d"%i)])

				x = x + 1

			else:
				_index.append(row[0])

				for i in range(1,len(row)):
					
					_data[i-1].append(row[i])	

	#print(_index[0:20])
	#print(_data[1][0:20])
	_index = mu.prepArray(_index)

	#print(len(_data[0]))
	#_dataNP = np.zeros((len(_data[0])-1,len(_data)-1))
	#for i in range(1,len(_data)):
	#	_dataNP[:,i-1] = np.asarray(_data[i][1:len(_data[0])])

	return [_index,_data]

#General purpose CSV importing function. Captures column headers, but does no processing on data block. Use for pulling in large blocks of unknown/unstructured data.
#Trust but verify.
def importArbitraryCSV_1(filename,headers = 0):

    _index = []
    _data = []

    x = 1

    with open(filename,'rt') as csvfile:
        data = csv.reader(csvfile, delimiter=',', quotechar='"');
        for row in data:

            #print(x)

            if (x < headers):

                headerContent = row[0].split(": ")

                if(headerContent[0] == "Scan Rate"):
                    print("File Properly Parsed")
                    #scanrate = float(headerContent[1])

                x = x + 1

            elif (x == headers):
                #Do Things for Table Header Row
                for i in range(1,len(row)):
                    _data.append([format("%s"%row[i])])

                x = x + 1

            else:
                _index.append(row[0])

                for i in range(1,len(row)):
                    
                    _data[i-1].append(row[i])   

    _index = mu.prepArray(_index)

    #_dataNP = np.zeros((len(_data[0])-1,len(_data)))
    _columnHeaders = []
    for i in range(0,len(_data)):
        #_dataNP[:,i] = np.asarray(_data[i][1:len(_data[0])])
        _columnHeaders.append(_data[i][0])

    return [_index,_data,_columnHeaders]

#CSV importing function specifically built for importing data from the MCDAQ devices Does not capture column headers, but 
#processes data block into numpy array. Use for pulling in data from MCDAQ csv files.
def import_MCDAQ_CSV(filename,headers = 7,verbose = True):

	_time = []
	_data = []

	x = 1

	with open(filename,'rt') as csvfile:
		data = csv.reader(csvfile, delimiter=',', quotechar='"');
		for row in data:

			#print(x)

			if (x < headers):

				headerContent = row[0].split(": ")

				if(headerContent[0] == "Scan Rate"):
					if(verbose):
						print("File Properly Parsed")
					scanrate = float(headerContent[1])

				x = x + 1

			elif (x == headers):
				#Do Things for Header Row
				for i in range(1,len(row)):
					_data.append([format("Column %d"%i)])

				x = x + 1

			else:
				_time.append(row[0])

				for i in range(1,len(row)):
					
					_data[i-1].append(row[i])	

	#print(_time[0:20])
	#print(_data[1][0:20])
	_time = mu.prepArray(_time)/scanrate

	#print(len(_data[0]))
	_dataNP = np.zeros((len(_data[0])-1,len(_data)-1))
	for i in range(1,len(_data)):
		_dataNP[:,i-1] = np.asarray(_data[i][1:len(_data[0])])

	return [_time,_dataNP]

#General purpose CSV importing function, tested and verified. Captures column headers, and does processing on data block to manipulate into
#numpy array. Use for pulling in large CSVs of numerical data that have headers.
def import_CSV_with_headers(filename,headers = 0):

	_index = []
	_data = []

	x = 1

	with open(filename,'rt') as csvfile:
		data = csv.reader(csvfile, delimiter=',', quotechar='"');
		for row in data:

			#print(x)

			if (x < headers):

				headerContent = row[0].split(": ")

				if(headerContent[0] == "Scan Rate"):
					print("File Properly Parsed")
					#scanrate = float(headerContent[1])

				x = x + 1

			elif (x == headers):
				#Do Things for Table Header Row
				for i in range(1,len(row)):
					_data.append([format("%s"%row[i])])

				x = x + 1

			else:
				_index.append(row[0])

				for i in range(1,len(row)):
					
					_data[i-1].append(row[i])   

	_index = mu.prepArray(_index)

	_dataNP = np.zeros((len(_data[0])-1,len(_data)))
	_columnHeaders = []
	for i in range(0,len(_data)):
		_dataNP[:,i] = np.asarray(_data[i][1:len(_data[0])])
		_columnHeaders.append(_data[i][0])

	return [_index,_dataNP,_columnHeaders]

#Example Instance of writeResults():
#	DDXfilename = 'C:/Users/Peter/ownCloud/Engineering/Food Production/Packaging Heat Transfer/DDX.csv'
#	rowsToWrite = np.asarray([DDX.T,Tambmax_1_5_F_1I.T]).T
#	writeResults(DDXfilename,rowsToWrite)
def writeResults(filename,rowsToWrite):
	
	with open(filename,'wt') as csvfile:
		data = csv.writer(csvfile, delimiter=',', quotechar='|');
		for i in range(0,len(rowsToWrite)):
			data.writerow(rowsToWrite[i,:])

def import_test(data):
	[time, temps] = [], []
	csvreader = csv.reader(StringIO(data))
	for row in enumerate(csvreader):
		try:
			if row[0] <= 1: # header lines
				pass
			else:
				time.append(row[1][0])
				temps.append([row[1][1], row[1][2]])
		except: pass
	return {
		"name": "test",
		"time": time,
		"temperatures": temps
	}

def import_channel_map(filename):
	printnote(1, "importing channel map")
	channel_map = {}
	with open(filename, "rt") as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',', quotechar='|');
		for row in enumerate(csvreader):
			try:
				channel_map.update({row[1][0]: {'Measurement Type': row[1][1], 'Index': row[1][2], 'RTD Index': row[1][3], 'Meals Per Box': row[1][4],\
					'Letter': row[1][5],'Ice Pack Count': row[1][6],'Protein Bottom/Top': row[1][7]}})
			except IndexError:
				channel_map.update({row[1][0]: row[1][1]})

	return channel_map

def import_hobo_csv(filename):
	printnote(1, "importing data from HOBO")
	TIMESTAMP_FMT = "%m/%d/%y %I:%M:%S %p"
	[time, temps, name] = [], [], None
	with open(filename, "rt") as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',', quotechar='|');
		for row in enumerate(csvreader):
			if row[0] == 0: # title line
				name = row[1][0].split(": ")[1].strip('"')#re.sub(r"^Plot Title: (.*)$", r"\1", row[1][0])
				printnote(2, 'name: "' + name + '"')
			elif row[0] == 1: # header line
				header = row[1][3].strip('"')#(row[1][3] + "," + row[1][4] + "," + row[1][5]).strip('"')
				printnote(2, "headers: " + str(header))
				temps.append({
					"name": header,
					"values": []
				})
			elif row[1][2]: # skip event entries
				time.append(int(datetime.timestamp(datetime.strptime(row[1][1], TIMESTAMP_FMT))))
				temps[0]["values"].append(float(row[1][2]))

	data = {
		"name": name,
		"time": time,
		"temperatures": temps
	}

	data["time"] = np.array(data["time"])

	for temp_set in enumerate(data["temperatures"]):
		data["temperatures"][temp_set[0]]["values"] \
				= np.array(data["temperatures"][temp_set[0]]["values"])

	return data

def import_room_monitor_csv(filename,sampleInterval = 60):
	printnote(1, "importing data from room monitor")
	[time, temps] = [], []

	with open(filename, "rt") as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',', quotechar='|');
		for row in enumerate(csvreader):
			if row[0] == 0: # PuTTY timestamp line
				ts_fields = row[1][0].split(" ")
				#print(ts_fields)
				ts_start = datetime.strptime(ts_fields[3] + " " + ts_fields[4],
					"%Y.%m.%d %H:%M:%S")
				printnote(2, "start time: " + str(ts_start))
				#print(datetime.timestamp(ts_start))
				ts_start = int(datetime.timestamp(ts_start))
			elif row[0] < 3: # status lines -- WARNING: may be more lines, depending what the firmware does
				pass
			elif row[0] == 3: # header line
				headers = row[1][:-1]
				printnote(2, "headers: " + str(headers))
				for header in headers:
					temps.append({
						"name": header.split(" ")[0] + " " +header.split(" ")[1],
						"values": []
					})
			else:	
				time.append(ts_start + int(row[1][-1]))
				for col in enumerate(row[1][:-1]):
					temps[col[0]]["values"].append(float(col[1]))
	
	data = {
		"name": "room monitor",
		"time": time,
		"temperatures": temps
	}
	reductionFactor = int(sampleInterval)

	data["time"] = tman.downSampleSignal(np.array(data["time"]),reductionFactor)

	for temp_set in enumerate(data["temperatures"]):
		data["temperatures"][temp_set[0]]["values"] \
				= tman.downSampleSignal(np.array(data["temperatures"][temp_set[0]]["values"]),reductionFactor)

	return data

def import_amalgamated_data_csv(filename):
	printnote(1, "importing data from amalgamated data file")

	[time, temps] = [], []
	with open(filename, "rt") as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',', quotechar='"');
		for row in enumerate(csvreader):
			if row[0] == 0: # header line
				headers = row[1][1:]
				printnote(2, "headers: " + str(headers))
				for header in headers:
					temps.append({
						"name": header,
						"values": []
					})
			else:
				time.append(float(row[1][0]))
				
				for col in enumerate(row[1][1:]):
					temps[col[0]]["values"].append(float(col[1]))
	data = {
		"name": "Amalgamated Data",
		"time": time,
		"temperatures": temps
	}

	return data

def import_daqami_csv(filename,tzOffset = 5,sampleInterval = 60):
	printnote(1, "importing data from DAQami")
	TIMESTAMP_FMT_SHORT = "%m/%d/%Y %I:%M:%S %p" # used when DAQami sample rate is > ~1 sec, probably
	TIMESTAMP_FMT_LONG = "%m/%d/%Y %I:%M:%S.%f %p" # used when DAQami sample rate is < ~1 sec, probably
	[time, temps] = [], []
	with open(filename, "rt") as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',', quotechar='|');
		for row in enumerate(csvreader):
			if row[0] == 0: # device name line
				daq_num = row[1][0].split(":")[1].strip('"').strip(' ') #re.sub(r"^Device: #([0-9]+) .*", r"\1", row[1][0])
			if row[0] == 4:
				ts_fields = row[1][0].split(" ")
				try:
					ts_start = datetime.strptime(ts_fields[2] + " " + ts_fields[3] + " " + ts_fields[4].strip('"'), TIMESTAMP_FMT_SHORT)
				except ValueError:
					try:
						ts_start = datetime.strptime(ts_fields[2] + " " + ts_fields[3] + " " + ts_fields[4].strip('"'), TIMESTAMP_FMT_LONG)
					except ValueError:
						printnote(2, "could not parse timestamp; timestamp format possibly incorrect", type = "error")
						return
				printnote(2, "start time: " + str(ts_start))
				ts_start = int(datetime.timestamp(ts_start))
			if row[0] == 5:
				scanrate = float(row[1][0].split(": ")[1].strip('"'))
			elif row[0] < 6: # misc. preamble
				pass
			elif row[0] == 6: # header line
				headers = row[1][2:]
				printnote(2, "headers: " + str(headers))
				for header in headers:
					temps.append({
						"name": "DAQ " + daq_num.split(' ')[0] + " Channel " + header.split(' ')[0].strip('"'),
						"values": []
					})
			else:
				time.append(ts_start + float(row[1][0].strip('"'))/scanrate)
				
				for col in enumerate(row[1][2:]):
					temps[col[0]]["values"].append(float(col[1].strip('""')))
	data = {
		"name": "DAQami",
		"time": time,
		"temperatures": temps
	}
	reductionFactor = int(sampleInterval*scanrate)

	data["time"] = tman.downSampleSignal(np.array(data["time"]),reductionFactor)

	for temp_set in enumerate(data["temperatures"]):
		data["temperatures"][temp_set[0]]["values"] \
				= tman.downSampleSignal(np.array(data["temperatures"][temp_set[0]]["values"]),reductionFactor)

	return data

def printnote(indent_level, message, type = "info"):
	tag = ""
	display = True
	if type == "info":
		display = VERBOSITY >= indent_level + 1
	elif type == "warning":
		display = VERBOSITY >= 1
	elif type == "error":
		tag = "ERROR: "
	if display:
		print("\t" * (indent_level - 1) + "• " + tag + message)