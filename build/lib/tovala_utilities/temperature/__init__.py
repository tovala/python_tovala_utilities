# -*- coding: utf-8 -*-
"""
Created on Wed May 24 11:18:51 2017

@author: Peter
"""

from __future__ import division
import csv
import numpy as np
import matplotlib.pylab as plt
import scipy.optimize as optimization
import scipy.signal as signalproc
import math
from .. import math_utility as mu
from .. import curve_fit as cf
from .. import intersection as intersect

coldT = 44
triggerTFall = 47
triggerTRise = 62
hotT = 69
nPts = 25

x0=np.array([2])

def FtoC(x):
    return 5/9*(x-32)

def CtoF(x):
    return (9/5*x+32)

def RTDvoltsToTempC(signal):
    return ((signal)*136.82-100)/0.385

def funcFall(x,b):
    return ((triggerTFall-coldT)*(np.exp(-b*(x)))+coldT)
    
def funcRise(x,b):
    return ((hotT-triggerTRise)*(1-np.exp(-b*(x)))+triggerTRise)
   
def fitTransitions(time,signal,triggerValFall,triggerValRise,blankingTime,ptSpread):
    transitionFit = np.asarray([])
    [indicesED,directionED] = mu.indexEdgeDetectRise(time,signal,triggerValRise,blankingTime)
    [indicesED1,directionED1] = mu.indexEdgeDetectFall(time,signal,triggerValFall,blankingTime)
    indicesED=np.append(indicesED,indicesED1)
    directionED=np.append(directionED,directionED1)
    for i in range(len(indicesED)):
        if(directionED[i]):
            opt=optimization.curve_fit(funcRise,(time[indicesED[i]-ptSpread:indicesED[i]+ptSpread]-time[indicesED[i]]),signal[indicesED[i]-ptSpread:indicesED[i]+ptSpread],x0,np.sqrt(np.abs(0.1*signal[indicesED[i]-ptSpread:indicesED[i]+ptSpread]-signal[indicesED[i]])))
        else:
            opt=optimization.curve_fit(funcFall,(time[indicesED[i]-ptSpread:indicesED[i]+ptSpread]-time[indicesED[i]]),signal[indicesED[i]-ptSpread:indicesED[i]+ptSpread],x0,np.sqrt(np.abs(0.1*signal[indicesED[i]-ptSpread:indicesED[i]+ptSpread]-signal[indicesED[i]])))
        fV=opt[0]
        #print fV
        transitionFit=np.append(transitionFit,fV)
    
    return [indicesED,directionED,transitionFit]

def heatingRate(time,data,temp,ptSpread): #Calculated in Degrees Per Second

    rate = mu.derivNearPoint(time,data,temp,ptSpread)

    return rate

def averageOscillationTemperature(time,data,last = "LastMinima",savgolFilterWindow = 0): #Root Library is Temperature Lib
    [indicesMax] = mu.detectLocalMaxima(data,savgolFilterWindow)
    [indicesMin] = mu.detectLocalMinima(data,savgolFilterWindow)
    #print(time[indicesMax])
    #print(time[indicesMin])
    indexMinLast = np.max(indicesMin[np.where(indicesMin<indicesMax[-1])])
    if(last == "LastMaxima"): #Calculates Average Temperature About the Last Maxima
        indexStop = 2*indicesMax[-1]-indexMinLast
        aOT = np.average(data[indexMinLast:indexStop])
        oMagnitude = np.abs(data[indexMinLast]-data[indicesMax[-1]])
        oPeriod = np.abs(time[indicesMax[-2]]-time[indicesMax[-1]])
    elif(last == "LastMinima"): #Calculates Average Temperature About the Last Minima
        aOT = np.average(data[indicesMax[-2]:indicesMax[-1]])
        oMagnitude = np.abs(data[indexMinLast]-data[indicesMax[-1]])
        oPeriod = np.abs(time[indicesMax[-2]]-time[indicesMax[-1]])
    elif(last == "MaxMinima"): #Calculate the Average Temperature About the Larger Minima Adjacent to the Largest Maxima
        indexAbsMax = np.argmax(data)
        indexMinBefore = np.max(indicesMin[np.where(indicesMin<indexAbsMax)])
        indexMinAfter = np.min(indicesMin[np.where(indicesMin>indexAbsMax)])
        oPeriod = np.abs(time[indexMinBefore]-time[indexMinAfter])
        if(data[indexMinBefore]>data[indexMinAfter]):
            indexStop = np.max(indicesMax[np.where(indicesMax<indexAbsMax)])
            aOT = np.average(data[indexStop:indexAbsMax])
            oMagnitude = np.abs(data[indexMinBefore]-data[indexAbsMax])
        else:
            indexStop = np.min(indicesMax[np.where(indicesMax>indexAbsMax)])
            aOT = np.average(data[indexAbsMax:indexStop])
            oMagnitude = np.abs(data[indexMinAfter]-data[indexAbsMax])
    elif(last == "FirstMinima"): #Calculates Average Temperature About the Last Minima
        aOT = np.average(data[indicesMax[0]:indicesMax[1]])
        indexMinFirst = np.max(indicesMin[np.where(indicesMin<indicesMax[1])])
        oMagnitude = np.abs(data[indexMinFirst]-data[indicesMax[1]])
        oPeriod = np.abs(time[indicesMax[0]]-time[indicesMax[1]])
    else:
        print("This Section of Average Oscillation Temperature Not Yet Built")
        return 0

    verificationIndices = [indexMinLast,indicesMax[-2],indicesMax[-1]]
    return [aOT,oMagnitude, oPeriod, verificationIndices]

def averageOscillationTemperatureR1(_time,_data,range = [0.5,0.95],spreadGuess = 20,oscPeriodGuess = 5, _timeConstGuess = 1, _method = "Mixed"): #From Time and Data, Get Avg Osc Temp, Osc Magnitude, and Osc Period for a Given Range
    datToFit = _data[math.floor(len(_time)*range[0]):math.floor(len(_time)*range[1])]
    tToFit = _time[math.floor(len(_time)*range[0]):math.floor(len(_time)*range[1])]
    avgT = np.average(datToFit)

    savgolFilterWindow = math.floor(len(datToFit)/6) % 2 + math.floor(len(datToFit)/6) + 1
    #print(savgolFilterWindow) 
    [indicesMax] = mu.detectLocalMaxima(datToFit,savgolFilterWindow)
    [indicesMin] = mu.detectLocalMinima(datToFit,savgolFilterWindow)
    #print(indicesMax)
    #rint(indicesMin)

    try:
        phaseGuess = (indicesMin[0]-indicesMax[0])/len(datToFit)*oscPeriodGuess
    except IndexError:
        phaseGuess = 0
        print("Could Not Properly Located Maxima/Minima")   

    if((_method == "FromFit") or (_method == "Mixed")):
        #Fit a Cycling Exponential to the Data, Use a 50% duty cycle and 0 phase as the default initial guess, expose the remainder as keyword arguments    
        [fitVar,fitSig] = cf.cyclingExponentialCurveFit(tToFit,datToFit,[oscPeriodGuess,0.5,phaseGuess,avgT-spreadGuess,avgT+spreadGuess,_timeConstGuess])
        oPeriod = fitVar[0]
    else:
        oPeriod = mu.findPeriod(tToFit,datToFit)

    indexEnd = math.floor(len(_time)*range[1])
    indexStart = np.max(np.where(_time<(_time[math.floor(len(_time)*range[1])]-2*oPeriod)))

    if(_method == "FromFit"):
        aOT = np.average(fitVar[3:5])
        oMagnitude = np.abs(fitVar[4]-fitVar[3])
        verification = [fitVar,fitSig]
    elif(_method == "Mixed"):
        aOT = np.average(_data[indexStart:indexEnd])
        oMagnitude = np.abs(fitVar[4]-fitVar[3])
        verification = [fitVar,fitSig]
    else: #_method == "DataAverage"
        aOT = np.average(_data[indexStart:indexEnd])
        oMagnitude = np.max(_data[indexStart:indexEnd])-np.min(_data[indexStart:indexEnd])
        verification = []
    
    return [aOT,oMagnitude, oPeriod, verification]

def preheatTime(LV_1X,LV_1Y,time,signal):
    [x,y] = intersect.intersection(LV_1X,LV_1Y,time,signal)
    if(len(y)==0):
        #print("No Preheat Time Found")
        phTime = -1
    else:
        #print("Preheat Time to %d = %0.2f" % (LV_1Y[0], x[0]))
        phTime = x[0]
        #print(y[0])        
            
    return phTime

def heaterPower(_time,_signal,_phTempArray,_heaterTimeConstantGuess = 1): #Calculate and Use Preheat Times to Return an Estimate of the Ultimate Oven Temperature and Preheat Time Constant
    phTimes = np.zeros(len(_phTempArray))
    for i in range(len(_phTempArray)):
        lv1X = np.asarray([0,20])
        lv1Y = np.asarray([_phTempArray[i],_phTempArray[i]])
        phTimes[i] = preheatTime(lv1X,lv1Y,_time,_signal)

    [fitVar,fitSig] = cf.expRiseCurveFit(phTimes,_phTempArray,[2*_phTempArray[-1],_phTempArray[-1],_heaterTimeConstantGuess])
    ultimateOvenTemp = fitVar[0]
    preheatTimeConst = fitVar[2]
    return [phTimes,ultimateOvenTemp,preheatTimeConst,fitSig]

def hotSpotAnalysis(_time, _tempArrays, avgOscTemp,method = "LastMinima",_savgolFilterWindow = 51,verbose = False):
    aotDiff = {}
    for key in _tempArrays:
        [aot,_,_,vIn] = averageOscillationTemperature(_time,_tempArrays[key],last = method,savgolFilterWindow = _savgolFilterWindow)
        aotDiff[key] = aot - avgOscTemp

        if(verbose):
            print(key)
            print(aotDiff[key])

    maxKey = max(iter(aotDiff.keys()), key=(lambda key: aotDiff[key]))
    minKey = min(iter(aotDiff.keys()), key=(lambda key: aotDiff[key]))

    return [maxKey,aotDiff[maxKey],minKey,aotDiff[minKey],vIn]

def hotSpotAnalysisR1(_time,_signal,_channelMap,_hscsGroup,avgOscTemp,_range = [0.5,0.95],verbose = False,_spreadGuess = 20,_oscPeriodGuess = 5, timeConstGuess = 1):
    aotDiff = {}
    for key in _channelMap:
        if key in _hscsGroup:
            [aot,_,_,_] = averageOscillationTemperatureR1(_time,_signal[:,_channelMap[key]],range = _range,spreadGuess = _spreadGuess,oscPeriodGuess = _oscPeriodGuess, _timeConstGuess = timeConstGuess)
            aotDiff[key] = aot - avgOscTemp
            
            if(verbose):
                print(key)
                print(aotDiff[key])

    maxKey = max(iter(aotDiff.keys()), key=(lambda key: aotDiff[key]))
    minKey = min(iter(aotDiff.keys()), key=(lambda key: aotDiff[key]))

    return [maxKey,aotDiff[maxKey],minKey,aotDiff[minKey]]