from __future__ import print_function
import csv
import numpy as np

N_SLEEVED_MEALS_PER_TOWER = 160
N_TRAYS_PER_TOWER = 320
N_SACHETS_PER_TOWER = 2000
N_CUPS_PER_TOWER = 1280

POSSIBLE_FROZEN_COMPONENTS = [
    "salmon",
    "pork",
    "chicken",
    "biscuit",
    "meatloaf"
]

PLATING_GUIDE_COLUMNS_MAPPING = {
    'v0': {
        'MealCode': 0,
        'MealCount': 1,
        'MealName': 2,
        "NTrays": 3,
        'SleevingLocation': 26,
        'FilmType': 27,
        'Tray1_Contents': 4,
        'Tray1_PreppedAt': 5,
        'Tray1_TransportedIn': 6, 
        'Tray1_PortionedAt': 7,
        'Tray1_MAP': 10,
        'Tray2_Contents': 11,
        'Tray2_PreppedAt': 12,
        'Tray2_TransportedIn': 13, 
        'Tray2_PortionedAt': 14,
        'Tray2_MAP': 17,
        'Garnish': 19,
        'Garnish_Container': 23,
        'Garnish_PreppedAt': 20,
        'Garnish_TransportedIn': 21,
        'Garnish_PortionedAt': 22    
    },
    'v1': {
        'MealCode': 0,
        'MealCount': 1,
        'MealName': 2,
        "NTrays": 3,
        'SleevingLocation': 32,
        'FilmType': 33,
        'Tray1_Contents': 4,
        'Tray1_PreppedAt': 5,
        'Tray1_PrepDay': 6,
        'Tray1_TransportedIn': 7, 
        'Tray1_PortionedAt': 8,
        'Tray1_PortionDay': 9,
        'Tray1_Weight': 10,
        'Tray1_MAP': 12,
        'Tray2_Contents': 13,
        'Tray2_PreppedAt': 14,
        'Tray2_PrepDay': 15,
        'Tray2_TransportedIn': 16, 
        'Tray2_PortionedAt': 17,
        'Tray2_PortionDay': 18,
        'Tray2_Weight': 19,
        'Tray2_MAP': 21,
        'Garnish': 23,
        'Garnish_Container': 29,
        'Garnish_PreppedAt': 24,
        'Garnish_PrepDay': 25,
        'Garnish_TransportedIn': 26,
        'Garnish_PortionedAt': 27,
        'Garnish_PortionDay': 28,
        'Garnish_Weight': 30    
    }
}

class MealObject:

    def __init__(self,_mealID,_mealName,_mealCount,_N_trays,_tray_1,_tray_2,_garnishes,_sleevingLocation,_filmType):
        self.mealID = _mealID
        self.mealName = _mealName
        if(_mealCount == ''):
            self.mealCount = 0
        else:
            self.mealCount = int(_mealCount)
        self.N_trays = _N_trays
        self.tray_1 = _tray_1
        self.tray_2 = _tray_2
        self.garnishes = _garnishes
        self.sleevingLocation = _sleevingLocation
        self.filmType = _filmType

class TrayObject:

    def __init__(self,_trayContents,_preppedAt,_transportedIn,_portionedAt,_map,_prepDay = 'None',_portionDay = 'None',_weight = "0"):
        self.trayContents = _trayContents
        self.preppedAt = _preppedAt
        self.prepDay = _prepDay
        self.transportedIn = _transportedIn
        self.portionedAt = _portionedAt
        self.portionDay = _portionDay
        self.map = _map
        self.weight = _weight

class GarnishObject:

    def __init__(self,_component,_preppedAt,_transportedIn,_portionedAt,_garnishType,_prepDay = 'None',_portionDay = 'None', _weight = "0"):
        self.component = _component
        self.preppedAt = _preppedAt
        self.prepDay = _prepDay
        self.transportedIn = _transportedIn
        self.portionedAt = _portionedAt
        self.portionDay = _portionDay
        self.garnishType = _garnishType
        self.weight = _weight

def import_plating_guide_csv(filename):
    print("Importing Plating Guide %s" % filename)

    plating_guide = []
    with open(filename, "rt") as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',', quotechar='"');
        for row in enumerate(csvreader):
            if row[0] == 0: # header line
                headers = row[1][:]
                #print("Headers: " + str(headers))
                for header in headers:
                    plating_guide.append({
                        "name": header,
                        "values": []
                    })
            else:
                
                for col in enumerate(row[1][:]):
                    plating_guide[col[0]]["values"].append(col[1])

    return plating_guide

def compileMealObjects(plating_guide, version = 'v0'):
    if version not in PLATING_GUIDE_COLUMNS_MAPPING:
        raise ValueError('Version Currently Not Supported By Plating Guide Parsing Library')
    meals = []
    for meal in enumerate(plating_guide[0]["values"]):
        if(meal[1] != ""):
            garnishes = []
            garnishType = ""

            if version == 'v1':
                tray1 = TrayObject([],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_PreppedAt']]["values"][meal[0]],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_TransportedIn']]["values"][meal[0]],\
                    plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_PortionedAt']]["values"][meal[0]],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_MAP']]["values"][meal[0]],\
                    _prepDay = plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_PrepDay']]["values"][meal[0]],_portionDay = plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_PortionDay']]["values"][meal[0]],\
                    _weight = "")

                tray2 = TrayObject([],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_PreppedAt']]["values"][meal[0]],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_TransportedIn']]["values"][meal[0]],\
                    plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_PortionedAt']]["values"][meal[0]],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_MAP']]["values"][meal[0]],\
                    _prepDay = plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_PrepDay']]["values"][meal[0]],_portionDay = plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_PortionDay']]["values"][meal[0]],\
                    _weight = "")

            else:
                tray1 = TrayObject([],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_PreppedAt']]["values"][meal[0]],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_TransportedIn']]["values"][meal[0]],\
                    plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_PortionedAt']]["values"][meal[0]],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_MAP']]["values"][meal[0]])
                tray2 = TrayObject([],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_PreppedAt']]["values"][meal[0]],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_TransportedIn']]["values"][meal[0]],\
                    plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_PortionedAt']]["values"][meal[0]],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_MAP']]["values"][meal[0]])

            for i in range(5):
                if(plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_Contents']]["values"][meal[0]+i] != "" and plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_Contents']]["values"][meal[0]+i] != "0"):
                    tray1.trayContents.append(plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_Contents']]["values"][meal[0]+i])
                    tray1.weight += " " + plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_Weight']]["values"][meal[0]+i]
                if(plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_Contents']]["values"][meal[0]+i] != "" and plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_Contents']]["values"][meal[0]+i] != "0"):
                    tray2.trayContents.append(plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_Contents']]["values"][meal[0]+i])
                    tray2.weight += " " + plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray2_Weight']]["values"][meal[0]+i]
                if(plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish']]["values"][meal[0]+i] != "" and plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish']]["values"][meal[0]+i] != "0"):
                    if("achet" in plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_Container']]["values"][meal[0]+i]):
                        garnishType = "Sachet"
                    elif("acket" in plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_Container']]["values"][meal[0]+i]):
                        garnishType = "Packet"
                    elif(("oz" in plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_Container']]["values"][meal[0]+i]) and ("1" in plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_Container']]["values"][meal[0]+i])):
                        garnishType = "1_oz_Cup"
                    elif(("oz" in plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_Container']]["values"][meal[0]+i]) and ("2" in plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_Container']]["values"][meal[0]+i])):
                        garnishType = "2_oz_Cup"
                    elif("ag" in plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_Container']]["values"][meal[0]+i]):
                        garnishType = "Bag"
                    else:
                        garnishType = "Unknown"

                    if version == 'v1':
                        garnishes.append(GarnishObject(plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish']]["values"][meal[0]+i],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_PreppedAt']]["values"][meal[0]+i],\
                            plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_TransportedIn']]["values"][meal[0]+i],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_PortionedAt']]["values"][meal[0]+i],garnishType,\
                            _prepDay = plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_PrepDay']]["values"][meal[0]+i],_portionDay = plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_PortionDay']]["values"][meal[0]+i],\
                            _weight = plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_Weight']]["values"][meal[0]+i]))

                    else:
                        garnishes.append(GarnishObject(plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish']]["values"][meal[0]+i],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_PreppedAt']]["values"][meal[0]+i],\
                            plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_TransportedIn']]["values"][meal[0]+i],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['Garnish_PortionedAt']]["values"][meal[0]+i],garnishType))

            meals.append(MealObject(meal[1],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['MealName']]["values"][meal[0]],plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['MealCount']]["values"][meal[0]],\
                plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['NTrays']]["values"][meal[0]],tray1,tray2,garnishes,plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['SleevingLocation']]["values"][meal[0]],\
                plating_guide[PLATING_GUIDE_COLUMNS_MAPPING[version]['FilmType']]["values"][meal[0]]))
        
    return meals

def totalMealCount(meals, includeOnly = []): #Function consumes a group of meals objects generated off of the plating guide and returns total meal count
    mealCount = 0
    for meal in meals:
        if(len(includeOnly) == 0 or meal.mealID in includeOnly):
            mealCount += meal.mealCount

    return mealCount

def isPortionedAtTubeway(obj):
    return obj.portionedAt == "Tubeway"

def isSleevedAtTubeway(mealID, meals):
    
    mealIDs = [meal.mealID for meal in meals]

    return meals[mealIDs.index(mealID)].sleevingLocation == "Tubeway"

def isSleevedAtPershing(mealID, meals):
    
    mealIDs = [meal.mealID for meal in meals]

    return meals[mealIDs.index(mealID)].sleevingLocation == "Pershing"

def isFrozenComponent(tray):
    return tray.transportedIn == "FC"

#Return whether or not a tray should be counted as transported from Pershing to Tubeway if part of a meal sleeved at Tubeway
def isTransportedTray(tray):
    transport = tray.trayContents != '' and not isFrozenComponent(tray)
    transport = transport and not isPortionedAtTubeway(tray)
    transport = transport and not "BAG" in tray.map
    transport = transport and not "VP" in tray.map

    if "CLM" in tray.map:
        print("WARNING: Clamshells Not Rated for Transport")

    return transport

#Build list of sleeved meal and frozen component towers for transport
def compileListOfTowers_SleevedMeals_FCs(meals,cycle = 0):
    towers = []
    for fc in POSSIBLE_FROZEN_COMPONENTS:
        towers.append({'id': fc, 'count': 0})

    for meal in meals:

        if(isSleevedAtPershing(meal.mealID, meals) and meal.mealCount > 0):
            if(cycle>0):
                name = str(meal.mealID) + " (Cycle " + str(cycle) + ")"
                towers.append({'id': name, 'count': np.ceil(meal.mealCount/N_SLEEVED_MEALS_PER_TOWER)})
            else:
                towers.append({'id': meal.mealID, 'count': np.ceil(meal.mealCount/N_SLEEVED_MEALS_PER_TOWER)})
        if(isFrozenComponent(meal.tray_1)):
            if (meal.tray_1.map != "VP"):
                for fc in POSSIBLE_FROZEN_COMPONENTS:
                    if(fc[1:] in meal.tray_1.trayContents[0]):
                        towerIDs = [tower['id'] for tower in towers]
                        towers[towerIDs.index(fc)]['count'] = towers[towerIDs.index(fc)]['count'] + meal.mealCount
            else:
                print("WARNING: Vac Packed Protein Tagged as Frozen Component, Meal %s" % meal.mealID)
        if(isFrozenComponent(meal.tray_2)):
            if (meal.tray_2.map != "VP"):
                for fc in POSSIBLE_FROZEN_COMPONENTS:
                    if(fc[1:] in meal.tray_2.trayContents[0]):
                        towerIDs = [tower['id'] for tower in towers]
                        towers[towerIDs.index(fc)]['count'] = towers[towerIDs.index(fc)]['count'] + meal.mealCount
            else:
                print("WARNING: Vac Packed Protein Tagged as Frozen Component, Meal %s" % meal.mealID)

    for fc in POSSIBLE_FROZEN_COMPONENTS:
        towerIDs = [tower['id'] for tower in towers]
        towers[towerIDs.index(fc)]['count'] = np.ceil(towers[towerIDs.index(fc)]['count']/N_TRAYS_PER_TOWER)

    return towers

#Build list of component towers for transport of meals to be sleeved at Tubeway
def compileListOfTowers_MealComponents(meals,cycle = 0):
    towers = []

    for meal in meals:

        if(isSleevedAtTubeway(meal.mealID, meals) and meal.mealCount > 0):

            if(cycle>0):
                cycle_append = ", Cycle " + str(cycle)
            else:
                cycle_append = ""

            #Add tray contents to tower list
            if(isTransportedTray(meal.tray_1)):
                towers.append({'id': meal.tray_1.trayContents[0] + "(" + meal.mealID + ")" + cycle_append, 'count': np.ceil(meal.mealCount/N_TRAYS_PER_TOWER)})
            if(isTransportedTray(meal.tray_2)):
                towers.append({'id': meal.tray_2.trayContents[0] + "(" + meal.mealID + ")" + cycle_append, 'count': np.ceil(meal.mealCount/N_TRAYS_PER_TOWER)})

            #Add garnishes to tower list
            for garnish in meal.garnishes:
                if(garnish.garnishType == "Sachet" and not isPortionedAtTubeway(garnish)):
                    towers.append({'id': garnish.component + "(" + meal.mealID + ")" + cycle_append, 'count': np.ceil(meal.mealCount/N_SACHETS_PER_TOWER)})
                if(garnish.garnishType == "Cup" and not isPortionedAtTubeway(garnish)):
                    towers.append({'id': garnish.component + "(" + meal.mealID + ")" + cycle_append, 'count': np.ceil(meal.mealCount/N_CUPS_PER_TOWER)})

    return towers