# -*- coding: utf-8 -*-
"""
Created on Wed May 24 11:18:51 2017

@author: Peter
"""

from __future__ import division
import csv
import numpy as np
import matplotlib.pylab as plt
import scipy.optimize as optimization
#import scipy.signal as signalproc
import pprint as pp
import math
import urllib3 as url
import json
import datetime

#TA_CDN_URL = 'http://cdn.tovala.com/assist/recipes.json'
TA_CDN_URL = 'http://cdn.dev.tovala.com/assist/recipes_press.json' #For 10/29/18 (week of) Press Tour

def getCDNData(routines = False, routineTotalTimes = False):
    
    #Make the Request and Return An Array of Recipe Names
    http = url.PoolManager()
    r = http.request('GET', TA_CDN_URL, headers={'Content-Type': 'application/json'})
    k = json.loads(r.data)['recipes']

    #Log Recipes
    _names = []
    _routineTotalTimes = []
    _routines = []
    totalTime = 0

    for thing in k:
        _names.append(thing['title'])
        if(routineTotalTimes):
            totalTime = 0
            a = thing['directionSections']
            for dS in a:
                #print("Checking Directions for Routine")
                b = dS['directions']
                for direction in b:
                    if 'routine' in direction:
                        if(routines):
                            _routines.append(direction['routine'])
                        for step in direction['routine']['routine']:
                            totalTime = totalTime + step['cookTime']

        _routineTotalTimes.append(totalTime)
        
        #print("Start Time(s) = %s" % (datetime.datetime.fromtimestamp(thing['_source']['timestamp']).strftime('%Y-%m-%d %H:%M:%S')))
        
    return [_names,_routines,_routineTotalTimes]