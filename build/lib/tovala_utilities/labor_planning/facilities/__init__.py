from __future__ import division
import numpy as np
import math
import json
import datetime
import copy
from .. import equipment as eq
from ..constants import *
#from .. import data_import as datIn

class FacilityNetwork:

     def __init__(self, _name, _facilityList,_scheduling = {}):
        self.facilityList = _facilityList
        self.scheduling = _scheduling

class Facility:

    def __init__(self, _name, _equipment_list,_storage_capacity):
        self.name = _name
        self.equipment_list = _equipment_list
        self.storage_capacity = _storage_capacity

def str_to_bool(s):
    if s == 'True':
         return True
    elif s == 'False':
         return False
    else:
         raise ValueError("String Could Not Convert to Boolean")

def buildFacilityNetwork(facility_network_JSON_filename, equipment_list_JSON_filename):
    with open(facility_network_JSON_filename) as f:
        fn_data = json.load(f)
    with open(equipment_list_JSON_filename) as f:
        eq_data = json.load(f)

    facs = []
    for fac in fn_data['facilities']:
        eq_list = []
        for equipment in fn_data['facilities'][fac]['equipment_list']:
            if(fn_data['facilities'][fac]['equipment_list'][equipment]['type'] in eq_data and str_to_bool(fn_data['facilities'][fac]['equipment_list'][equipment]['active'])):
                eq_type = fn_data['facilities'][fac]['equipment_list'][equipment]['type']
                
                process_list = []
                ct = 0
                for process in eq_data[eq_type]['process_list']:
                    if("changeover_time" in eq_data[eq_type]['process_list'][process]):
                        ct = eq_data[eq_type]['process_list'][process]["changeover_time"]
                    efficiency = 0.8
                    if("efficiency" in fn_data['facilities'][fac]['equipment_list'][equipment]):
                        efficiency = fn_data['facilities'][fac]['equipment_list'][equipment]["efficiency"]

                    process_list.append(eq.ProcessObject(process,eq_data[eq_type]['process_list'][process]["N_workers"],\
                        eq_data[eq_type]['process_list'][process]["max_capacity_per_hour"],eq_data[eq_type]['process_list'][process]["output_unit"],_efficiency = efficiency,_changeover_time = ct))
                for process in eq_data[eq_type]['batch_process_list']:
                    if("changeover_time" in eq_data[eq_type]['batch_process_list'][process]):
                        ct = eq_data[eq_type]['batch_process_list'][process]["changeover_time"]
                    process_list.append(eq.BatchProcessObject(process,eq_data[eq_type]['batch_process_list'][process]["N_workers"],\
                        eq_data[eq_type]['batch_process_list'][process]["max_weight_per_batch"],eq_data[eq_type]['batch_process_list'][process]["output_unit"],_changeover_time = ct))
                if('available_line_hours' in fn_data['facilities'][fac]['equipment_list'][equipment]):
                    alh = fn_data['facilities'][fac]['equipment_list'][equipment]['available_line_hours']
                    if 'visualization_cells' in fn_data['facilities'][fac]['equipment_list'][equipment]:
                        eq_list.append({'name': equipment, 'equipment': eq.EquipmentObject(process_list, _type = eq_data[eq_type]['type'], _available_line_hours = \
                            eq.AvailableLineHours(alh['one_standard'],alh['one_stretch'],alh['two_standard'],alh['two_stretch'],alh['two_max']),\
                            _visualization = fn_data['facilities'][fac]['equipment_list'][equipment]['visualization_cells'])})
                    else:
                        eq_list.append({'name': equipment, 'equipment': eq.EquipmentObject(process_list, _type = eq_data[eq_type]['type'], _available_line_hours = \
                            eq.AvailableLineHours(alh['one_standard'],alh['one_stretch'],alh['two_standard'],alh['two_stretch'],alh['two_max']))})

                else:
                    if 'visualization_cells' in fn_data['facilities'][fac]['equipment_list'][equipment]:
                        eq_list.append({'name': equipment, 'equipment': eq.EquipmentObject(process_list, _type = eq_data[eq_type]['type'], _visualization = fn_data['facilities'][fac]['equipment_list'][equipment]['visualization_cells'])})
                    else:
                        eq_list.append({'name': equipment, 'equipment': eq.EquipmentObject(process_list, _type = eq_data[eq_type]['type'])})

        facs.append(Facility(fac,eq_list,fn_data['facilities'][fac]['storage_cap']))
        

    PRODUCTION_NETWORK = FacilityNetwork(fn_data['name'],facs,fn_data['scheduling_options'])

    return PRODUCTION_NETWORK