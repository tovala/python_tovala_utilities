from __future__ import division
import numpy as np
import math
import urllib3 as url
import json
import datetime
import copy
from .. import equipment as eq
from ..constants import *
#from .. import data_import as datIn


def next_weekday(d,weekday):
	days_ahead = weekday - d.weekday()
	if(days_ahead <= 0):
		days_ahead +=7
	return d + datetime.timedelta(days_ahead)


def equipment_scheduler(task,task_equipment_options,_facility_network,counters = {'TRAY_PORTIONING': 0,'GARNISH_PORTIONING': 0,'SLEEVING':0},verbosity = VL_WARNING):

	if(verbosity <= VL_DEBUG):
		print(task.name)

	equipment_assigned = []

	if task.process == "PACKOUT":
		if("Packout" not in _facility_network.scheduling):
			_facility_network.scheduling['Packout'] = 'LESS_THAN_10_HOURS_LINE_TIME'
		if(_facility_network.scheduling['Packout'] == 'LESS_THAN_10_HOURS_LINE_TIME'):
			rate_per_hour = 0
			for equipment in task_equipment_options:
				equipment_assigned.append(equipment['name'])
				eq_process = next(process for process in equipment['equipment'].processList if process.name == task.process)
				rate_per_hour += eq_process.rate_per_hour
				
				if(task.totalCount/rate_per_hour < 10):
					break

	if task.process == "SLEEVING":
		if("Sleeving" not in _facility_network.scheduling):
			_facility_network.scheduling['Sleeving'] = '2_STATIONS_ROTATING'
		if(_facility_network.scheduling['Sleeving'] == '2_STATIONS_ROTATING'):
			equipment_assigned.append(task_equipment_options[counters['SLEEVING'] % len(task_equipment_options)]['name'])
			counters['SLEEVING'] += 1
			equipment_assigned.append(task_equipment_options[counters['SLEEVING'] % len(task_equipment_options)]['name'])
			counters['SLEEVING'] += 1

	if task.process == "BAND_SEALING":
		if("Band_Sealing" not in _facility_network.scheduling):
			_facility_network.scheduling['Band_Sealing'] = 'MAX_CAPACITY'
		if(_facility_network.scheduling['Band_Sealing'] == 'MAX_CAPACITY'):
			for equipment in task_equipment_options:
				equipment_assigned.append(equipment['name'])

	if task.process == "SACHET_DEPOSITING":
		if("Sachet_Depositing" not in _facility_network.scheduling):
			_facility_network.scheduling['Sachet_Depositing'] = 'MAX_CAPACITY'
		if(_facility_network.scheduling['Sachet_Depositing'] == 'MAX_CAPACITY'):
			for equipment in task_equipment_options:
				equipment_assigned.append(equipment['name'])

	if task.process == "TRAY_SEALING":
		if("Tray_Sealing" not in _facility_network.scheduling):
			_facility_network.scheduling['Tray_Sealing'] = '5_MACHINES_MAX'
		if(_facility_network.scheduling['Tray_Sealing'] == '5_MACHINES_MAX'):
			counter = 0
			for equipment in task_equipment_options:
				equipment_assigned.append(equipment['name'])
				counter += 1
				if counter == 5:
					break

	if task.process == "TRAY_PORTIONING":
		if("Tray_Portioning" not in _facility_network.scheduling):
			_facility_network.scheduling['Tray_Portioning'] = 'SINGLE_EQUIPMENT_ROTATING'
		if(_facility_network.scheduling['Tray_Portioning'] == 'SINGLE_EQUIPMENT_ROTATING'):

			equipment_assigned.append(task_equipment_options[counters['TRAY_PORTIONING'] % len(task_equipment_options)]['name'])
			counters['TRAY_PORTIONING'] += 1

	if task.process == "GARNISH_CUP_PORTIONING_SEALING":
		if("Garnish_Portioning" not in _facility_network.scheduling):
			_facility_network.scheduling['Garnish_Portioning'] = 'SINGLE_EQUIPMENT_ROTATING'
		if(_facility_network.scheduling['Garnish_Portioning'] == 'SINGLE_EQUIPMENT_ROTATING'):

			equipment_assigned.append(task_equipment_options[counters['GARNISH_PORTIONING'] % len(task_equipment_options)]['name'])
			counters['GARNISH_PORTIONING'] += 1

	if task.process == "OVEN":
		if("Oven" not in _facility_network.scheduling):
			_facility_network.scheduling['Oven'] = 'UNOXES_ONLY'
		if(_facility_network.scheduling['Oven'] == 'UNOXES_ONLY'):
			counter = 0
			for equipment in task_equipment_options:
				if("Unox" in equipment['name']):
					equipment_assigned.append(equipment['name'])
				counter += 1
				if counter == 5:
					break

	if task.process == "OVEN_MEATBALL":
		if("Oven Meatball" not in _facility_network.scheduling):
			_facility_network.scheduling['Oven Meatball'] = 'UNOXES_ONLY'
		if(_facility_network.scheduling['Oven Meatball'] == 'UNOXES_ONLY'):
			counter = 0
			for equipment in task_equipment_options:
				if("Unox" in equipment['name']):
					equipment_assigned.append(equipment['name'])
				counter += 1
				if counter == 5:
					break

	if task.process == "FORMING_MEATBALL":
		if("Forming Meatball" not in _facility_network.scheduling):
			_facility_network.scheduling['Forming Meatball'] = '5_MACHINES_MAX'
		if(_facility_network.scheduling['Forming Meatball'] == '5_MACHINES_MAX'):
			counter = 0
			for equipment in task_equipment_options:
				equipment_assigned.append(equipment['name'])
				counter += 1
				if counter == 5:
					break

	if task.process == "MIX_BATCH":
		if("Batch_Mix" not in _facility_network.scheduling):
			_facility_network.scheduling['Batch_Mix'] = 'SINGLE_TUMBLER_ONLY'
		if(_facility_network.scheduling['Batch_Mix'] == 'SINGLE_TUMBLER_ONLY'):
			for equipment in task_equipment_options:
				if("Tumbler" in equipment['name']):
					equipment_assigned.append(equipment['name'])
					break

	if task.process == "SKILLET":
		if("Skillet" not in _facility_network.scheduling):
			_facility_network.scheduling['Skillet'] = 'SINGLE_SKILLET_ONLY'
		if(_facility_network.scheduling['Skillet'] == 'SINGLE_SKILLET_ONLY'):
			for equipment in task_equipment_options:
				if("Skillet" in equipment['name']):
					equipment_assigned.append(equipment['name'])
					break

	if task.process == "MIX_SAUCE":
		if("Sauce_Mix" not in _facility_network.scheduling):
			_facility_network.scheduling['Sauce_Mix'] = 'SINGLE_BLENDER_ONLY'
		if(_facility_network.scheduling['Sauce_Mix'] == 'SINGLE_BLENDER_ONLY'):
			for equipment in task_equipment_options:
				if("Blender" in equipment['name']):
					equipment_assigned.append(equipment['name'])
					break

	if task.process == "KETTLE":
		if("Kettle" not in _facility_network.scheduling):
			_facility_network.scheduling['Kettle'] = 'MIN_BATCHES_MIN_CAPACITY'
		if(_facility_network.scheduling['Kettle'] == 'MIN_BATCHES_MIN_CAPACITY'):

			kettle_options = []
			for equipment in task_equipment_options:
				kettle_options.append({'name': equipment['name'], 'batch_size':  next(process for process in equipment['equipment'].processList if process.name == task.process).max_weight_per_batch})

			kettle_options = sorted(kettle_options, key = lambda i: (i['batch_size']))

			for i in range(len(kettle_options)):
				if task.totalCount < kettle_options[i]['batch_size']:
					equipment_assigned.append(kettle_options[i]['name'])
					break

			if (len(equipment_assigned) == 0 and len(kettle_options)>1):
				for i in range(len(kettle_options)-1):
					if task.totalCount < kettle_options[i]['batch_size'] + kettle_options[i+1]['batch_size']:
						equipment_assigned.extend([kettle_options[i]['name'],kettle_options[i+1]['name']])
						break
			elif(len(equipment_assigned) == 0):
				equipment_assigned.append(kettle_options[0]['name'])

			if len(equipment_assigned) == 0:
				equipment_assigned.extend([kettle_options[-2]['name'],kettle_options[-1]['name']])

	if task.process == "THAW":
		if("Thaw" not in _facility_network.scheduling):
			_facility_network.scheduling['Thaw'] = 'FIRST_AVAILABLE'
		if(_facility_network.scheduling['Thaw'] == 'FIRST_AVAILABLE'):

			for equipment in task_equipment_options:
				equipment_assigned.append(equipment['name'])
				break

	if task.process == "OPEN":
		if("Open" not in _facility_network.scheduling):
			_facility_network.scheduling['Open'] = 'FIRST_AVAILABLE'
		if(_facility_network.scheduling['Open'] == 'FIRST_AVAILABLE'):

			for equipment in task_equipment_options:
				equipment_assigned.append(equipment['name'])
				break

	if task.process == "DRAIN":
		if("Drain" not in _facility_network.scheduling):
			_facility_network.scheduling['Drain'] = 'FIRST_AVAILABLE'
		if(_facility_network.scheduling['Drain'] == 'FIRST_AVAILABLE'):

			for equipment in task_equipment_options:
				equipment_assigned.append(equipment['name'])
				break

	if task.process == "FACILITY":
		if("Facility" not in _facility_network.scheduling):
			_facility_network.scheduling['Facility'] = 'FIRST_AVAILABLE'
		if(_facility_network.scheduling['Facility'] == 'FIRST_AVAILABLE'):

			for equipment in task_equipment_options:
				equipment_assigned.append(equipment['name'])
				break


	task.assigned_equipment = equipment_assigned
	for i in range(5):
		if len(task.assigned_equipment)<5:
			task.assigned_equipment.append("")

	return [task,counters]