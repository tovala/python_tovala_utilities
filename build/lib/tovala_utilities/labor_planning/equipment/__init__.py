from __future__ import division
import numpy as np
import math
import datetime
import copy
from ..constants import *
import google_sheets as gs
#from .. import data_import as datIn

class AvailableLineHours:

    def __init__(self, standard1 = 8, stretch1 = 10, standard2 = 14, stretch2 = 17, max2 = 21):
        self.available_line_hours = {'one_standard': standard1, 'one_stretch': 10, 'two_standard': 14, 'two_stretch': stretch2, 'two_max': max2}

    def load(self, req_line_hours):
        if(req_line_hours < self.available_line_hours['one_standard']):
            return "Dark Green"
        elif(req_line_hours < self.available_line_hours['one_stretch']):
            return "Light Green"
        elif(req_line_hours < self.available_line_hours['two_standard']):
            return "Yellow"
        elif(req_line_hours < self.available_line_hours['two_stretch']):
            return "Orange"
        elif(req_line_hours < self.available_line_hours['two_max']):
            return "Light Red"
        else:
            return "Dark Red"


TOVALA_STANDARD_ALHs = AvailableLineHours()
class EquipmentObject:

    def __init__(self,_processList,_available_line_hours=TOVALA_STANDARD_ALHs, _visualization = [],_type = ""):
        self.type = _type
        self.available_line_hours = _available_line_hours
        self.visualization = _visualization
        self.processList = []
        self.utilization = {'Sunday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR)),\
                             'Monday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR)),\
                             'Tuesday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR)),\
                             'Wednesday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR)),\
                             'Thursday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR)),\
                             'Friday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR)),\
                             'Saturday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR))
                            }
        for process in _processList:
            if(process.name in validProcesses):
                self.processList.append(process)
            else:
                print("WARNING: PROCESS \"%s\" NOT RECOGNIZED AS A VALID PROCESS, IGNORING" % process.name)

    def load(self):
        for day in self.utilization:
            self.utilization[day].load = self.available_line_hours.load(self.utilization[day].line_hours)

    #def resetUtilization(self):
    #    self.utilization = {'Sunday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR)),\
    #                         'Monday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR)),\
    #                         'Tuesday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR)),\
    #                         'Wednesday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR)),\
    #                         'Thursday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR)),\
    #                         'Friday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR)),\
    #                         'Saturday': UtilizationObject(0,copy.deepcopy(DEFAULT_COLOR))
    #                       }

class ProcessObject:

    def __init__(self, _name, _N_workers, _max_capacity_per_hour, _output_unit,_efficiency = 0.8,_changeover_time = 0):
        self.name = _name
        self.N_workers = _N_workers
        self.max_capacity_per_hour = _max_capacity_per_hour #Machine/Line Capacity, for Actual Output per Hour
        if(_output_unit in validOutputUnits):
            self.output_unit = _output_unit
        else:
            raise ValueError('Output Unit of Process Not Recognized')
        self.rate_per_hour = _efficiency*_max_capacity_per_hour
        self.changeover_time = _changeover_time

class BatchProcessObject:

    def __init__(self, _name, _N_workers, _max_weight_per_batch, _output_unit,_changeover_time = 0):
        self.name = _name
        self.N_workers = _N_workers
        self.max_weight_per_batch = _max_weight_per_batch #Machine/Line Capacity, for Weight per Batch
        if(_output_unit in validOutputUnits):
            self.output_unit = _output_unit
        else:
            raise ValueError('Output Unit of Process Not Recognized')
        self.changeover_time = _changeover_time

class UtilizationObject:

    def __init__(self, _line_hours, _load):
        self.line_hours = _line_hours
        self.load = _load
        self.taskList = [] #Summary of tasks by day consisting of task title and line hours required

    def add_task(self, name, line_hours_reqd, workers_reqd):
        self.taskList.append({'name': name, 'line_hours': line_hours_reqd, 'workers': workers_reqd})

def colorEquipment(facility_name, equipment, day, service, spreadsheet_id):
    if(len(equipment['equipment'].visualization) == 0):
        print("WARNING: Not Coloring Heat Map for %s %s" % (facility_name, equipment['name']))
    else:
        gs.colorCellRange(facility_name+": "+day, equipment['equipment'].utilization[day].load, equipment['equipment'].visualization[0],\
            equipment['equipment'].visualization[1], equipment['equipment'].visualization[2], equipment['equipment'].visualization[3], service, spreadsheet_id)
