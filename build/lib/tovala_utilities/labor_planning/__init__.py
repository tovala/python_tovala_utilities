from __future__ import division
import numpy as np
import math
import json
import urllib3 as url
import datetime
import google_sheets as gs
from . import facilities
from . import equipment as eq
from .constants import *
from . import tasks as TKS
from . import scheduler

def NIST_MAV(weight): #Weight spec'd in grams
    if(weight < 36):
        return weight*0.1
    elif(weight < 54):
        return 3.6
    elif(weight < 81):
        return 5.4
    elif(weight < 117):
        return 7.2
    elif(weight < 154):
        return 9.0
    elif(weight < 208):
        return 10.8
    elif(weight < 263):
        return 12.7
    elif(weight < 317):
        return 14.5
    elif(weight < 318):
        return 16.3
    elif(weight < 426):
        return 18.1
    elif(weight < 489):
        return 19.9
    elif(weight < 571):
        return 21.7
    elif(weight < 635):
        return 23.5
    elif(weight < 698):
        return 25.4
    else:
        return 0.0

def getFullProductionRecord(term, env = 'prod'):

	#Make the Request and Return Only the Documents Requested
	http = url.PoolManager()
	if(env == 'dev'):
		r = http.request('GET', DEV_PRODUCTION_RECORD_URL + str(term))
	else:
		r = r = http.request('GET', PRODUCTION_RECORD_URL + str(term))
	k = json.loads(r.data)

	return k

def compileTasks(productionRecord, scaleMealCount = 0, cycleMethod = "PERCENTAGE_SPLIT", cycleSplit = {"1": 1.0,"2": 0.0}, _verbosity = VL_DEBUG):

    tasks = []

    #Determine if the method for compiling tasks is a recognized method. If not, raise an error.
    if cycleMethod not in ALLOWABLE_CYCLE_METHODS:
        raise ValueError("Unrecognized Cycle Method for Compiling Tasks")

    #If the method for compiling tasks is by a percentage split for planning purposes, take the existing projection for cycle 1 meals,
    #and use the cycleSplit input to allocate tasks by cycle for a full meal count of either the total meal count for cycle 1 only,
    #or a scaled meal count.
    if cycleMethod == "PERCENTAGE_SPLIT":

        #Normalize Cycles to Total Meal Count
        cycleSplitTot = sum(cycleSplit.values(), 0.0)
        cycleSplit = {k: v/cycleSplitTot for k,v in cycleSplit.items()}

        cycles = []
        for cycle in cycleSplit:
            cycles.append(cycle)

    #If the method for compiling tasks is by actual meal counts by cycle, do so, allowing for scaling the current numbers up to any
    #given meal count,
    elif cycleMethod == "BY_ACTUAL_CYCLE_COUNT":

        cycles = [cycle for cycle in productionRecord['cycles']]

    #Determine the total meal count before scaling (for PERCENTAGE_SPLIT this will be the cycle 1 total meal count, for BY_ACTUAL_CYCLE_COUNT,
    #this is the accurate total meal count)
    totalMealCount = 0
    for cycle in cycles:
        totalMealCount += sum(meal['totalMeals'] for meal in productionRecord['cycles'][cycle]['meals'])
        if cycleMethod == "PERCENTAGE_SPLIT":
            break

    if(_verbosity <= VL_INFORMATIONAL):
        print("Total Meal Count Before Scaling: %0d" % totalMealCount)
    originalMealCount = totalMealCount


    #If needed, scale the meal count
    if(scaleMealCount != 0):
        for cycle in cycles:
            for meal in productionRecord['cycles'][cycle]['meals']:
                meal['totalMeals'] = int(meal['totalMeals']*scaleMealCount/totalMealCount)
            if cycleMethod == "PERCENTAGE_SPLIT":
                break
        totalMealCount = scaleMealCount

    if(_verbosity <= VL_DEBUG):
        print("Total Meal Count: %0d" % totalMealCount)

    #Iterate over cycles to compile the full task list
    for cycle in cycles:

        if cycleMethod == "PERCENTAGE_SPLIT": #When calculating by PERCENTAGE_SPLIT,
            meals = productionRecord['cycles']["1"]['meals'] #use the meals (and counts) for Cycle 1
            parts = productionRecord['cycles']["1"]['parts'] #use the parts (and counts) for Cycle 1
            cycleMealCount = totalMealCount*cycleSplit[cycle] #for cycle meal count, take the total meal count and multiply by the split for each cycle
            multiplier = cycleSplit[cycle] #multiply those counts by the split for each cycle
        elif cycleMethod == "BY_ACTUAL_CYCLE_COUNT":
            meals = productionRecord['cycles'][cycle]['meals'] #use the meals (and counts) for each cycle
            parts = productionRecord['cycles'][cycle]['parts'] #use the parts (and counts) for each cycle
            cycleMealCount = sum(meal['totalMeals'] for meal in meals) #for cycle meal count, take the actual meal count for each cycle
            multiplier = 1.0 #since correct counts are used, multiply those counts by 1

        #---------------------------------------------------------------------------------------------------------
        #Compile Packout Tasks
        #---------------------------------------------------------------------------------------------------------

        tasks.append(TKS.Task(cycle + " Cycle Packout|Day 1",'PACKOUT','BOXES',FRAC_1ST_PACKOUT_DAY*cycleMealCount/N_MEALS_PER_BOX_AVG,'Tubeway'))
        tasks[-1].dailyCount[CYCLE_SHIP_DAY_MAP[cycle]] += tasks[-1].totalCount
        tasks.append(TKS.Task(cycle + " Cycle Packout|Day 2",'PACKOUT','BOXES',FRAC_2ND_PACKOUT_DAY*cycleMealCount/N_MEALS_PER_BOX_AVG,'Tubeway'))
        tasks[-1].dailyCount[DAYS[next(day[0] for day in enumerate(DAYS) if day[1] == CYCLE_SHIP_DAY_MAP[cycle])+1]] += tasks[-1].totalCount

        for meal in meals:
            tray1_sealed = False
            tray2_sealed = False
            if meal['totalMeals']>0:

                #---------------------------------------------------------------------------------------------------------
                #Check for and Add Meatballs:
                #---------------------------------------------------------------------------------------------------------
                if(('eatball' in meal['partTitle']) or ('eatball' in meal['apiMealTitle']) or ('eatball' in meal['shortTitle'])):
                    tasks.append(TKS.Task(cycle + " Cycle Prep, Oven|Meatballs (Meal " + str(meal['mealCode']) + ")",'OVEN_MEATBALL','EACH',meal['totalMeals']*4*multiplier,'Pershing',_hours_per_batch = 0.5,_meal = meal['mealCode'],_parent = meal['partTitle']))
                    tasks[-1].dailyCount['Wednesday'] += tasks[-1].totalCount
                    tasks.append(TKS.Task(cycle + " Cycle Prep, Forming|Meatball Forming (Meal " + str(meal['mealCode']) + ")",'FORMING_MEATBALL','EACH',meal['totalMeals']*4*multiplier,'Pershing',_hours_per_batch = 0.5,_meal = meal['mealCode'],_parent = meal['partTitle']))
                    tasks[-1].dailyCount['Tuesday'] += tasks[-1].totalCount



                #---------------------------------------------------------------------------------------------------------
                #Compile Sleeving Tasks
                #---------------------------------------------------------------------------------------------------------

                sleevingLoc = ""
                if 'pershing' in next(tag['title'] for tag in meal['tags'] if tag['category'] == "sleeving_location"):
                    sleevingLoc = "Pershing"
                elif 'tubeway' in next(tag['title'] for tag in meal['tags'] if tag['category'] == "sleeving_location"):
                    sleevingLoc = "Tubeway"

                tasks.append(TKS.Task(cycle + " Cycle Sleeving|Meal " + str(meal['mealCode']),'SLEEVING','MEALS',meal['totalMeals']*multiplier,sleevingLoc,_meal = str(meal['mealCode'])))

                #Currently scheduling all sleeving for Sunday Cycle on Friday
                tasks[-1].dailyCount[DAYS[next(day[0] for day in enumerate(DAYS) if day[1] == CYCLE_SHIP_DAY_MAP[cycle])-2]] += tasks[-1].totalCount

                #---------------------------------------------------------------------------------------------------------
                #Compile Portioning Tasks
                #---------------------------------------------------------------------------------------------------------

                for component in meal['billOfMaterials']:
                    if(_verbosity <= VL_INFORMATIONAL):
                        print(component['title'])

                    try: 
                        portion_day = next(tag['title'] for tag in component['tags'] if tag['category'] == 'portion_date').capitalize()
                    except StopIteration:
                        portion_day = 'Monday'
                    try:
                        portion_loc = next(tag['title'] for tag in component['tags'] if tag['category'] == 'portion_location').capitalize()
                    except StopIteration:
                        portion_loc = 'Pershing'
                    try:
                        tray_tag = next(tag['title'] for tag in component['tags'] if tag['category'] == 'container')
                    except StopIteration:
                        tray_tag = '2 oz cup'


                    try:
                        if(tray_tag == 'sachet'):
                            tasks.append(TKS.Task(cycle + " Cycle Prep, Meal " + str(meal['mealCode']) + ", Garnish|" + component['title'],'SACHET_DEPOSITING','SACHETS',meal['totalMeals']*multiplier,\
                                portion_loc,_meal = str(meal['mealCode']),_parent = meal['apiMealTitle'], _target_weight = component['qtyPounds']*GRAMS_PER_LB))
                            if(portion_day in DAYS):
                                tasks[-1].dailyCount[portion_day] += tasks[-1].totalCount
                            if(_verbosity <= VL_INFORMATIONAL):
                                print("Added Sachet to %s on %s" % (portion_loc,portion_day))
                        if(tray_tag == '2 oz cup'):
                            tasks.append(TKS.Task(cycle + " Cycle Prep, Meal " + str(meal['mealCode']) + ", Garnish|" + component['title'],'GARNISH_CUP_PORTIONING_SEALING','2_OZ_CUPS',meal['totalMeals']*multiplier,\
                                portion_loc,_meal = str(meal['mealCode']),_parent = meal['apiMealTitle'], _target_weight = component['qtyPounds']*GRAMS_PER_LB))
                            if(portion_day in DAYS):
                                tasks[-1].dailyCount[portion_day] += tasks[-1].totalCount
                            if(_verbosity <= VL_INFORMATIONAL):
                                print("Added 2 oz Cup to %s on %s" % (portion_loc,portion_day))
                        if(tray_tag == '1 oz cup'):
                            tasks.append(TKS.Task(cycle + " Cycle Prep, Meal " + str(meal['mealCode']) + ", Garnish|" + component['title'],'GARNISH_CUP_PORTIONING_SEALING','1_OZ_CUPS',meal['totalMeals']*multiplier,\
                                portion_loc,_meal = str(meal['mealCode']),_parent = meal['apiMealTitle'], _target_weight = component['qtyPounds']*GRAMS_PER_LB))
                            
                            if(portion_day in DAYS):
                                tasks[-1].dailyCount[portion_day] += tasks[-1].totalCount
                            if(_verbosity <= VL_INFORMATIONAL):
                                print("Added 1 oz Cup to %s on %s" % (portion_loc,portion_day))
                        if('vac pack' in tray_tag):
                            continue
                        if('bag' in tray_tag):
                            tasks.append(TKS.Task(cycle + " Cycle Prep, Meal " + str(meal['mealCode']) + ", Bag|" + component['title'],'BAND_SEALING','BAGS',meal['totalMeals']*multiplier,\
                                portion_loc,_meal = str(meal['mealCode']),_parent = meal['apiMealTitle'], _target_weight = component['qtyPounds']*GRAMS_PER_LB))
                            if(portion_day in DAYS):
                                tasks[-1].dailyCount[portion_day] += tasks[-1].totalCount
                            if(_verbosity <= VL_INFORMATIONAL):
                                print("Added Bag to %s on %s" % (portion_loc,portion_day))
                            continue
                        if('clamshell' in tray_tag):
                            trayTitle = next(tag['title'] for tag in component['tags'] if tag['category'] == 'container')
                            tasks.append(TKS.Task(cycle + " Cycle Prep, Meal " + str(meal['mealCode']) + "|" + component['title'] + " (clamshell)",'TRAY_PORTIONING','CLAMSHELLS',meal['totalMeals']*multiplier,\
                                portion_loc,_meal = str(meal['mealCode']),_parent = meal['apiMealTitle'], _target_weight = component['qtyPounds']*GRAMS_PER_LB))
                            if(portion_day in DAYS):
                                tasks[-1].dailyCount[portion_day] += tasks[-1].totalCount
                            if(_verbosity <= VL_INFORMATIONAL):
                                print("Added Tray %s on %s" % (portion_loc,portion_day))
                            continue
                        if('tray' in tray_tag):
                            trayTitle = tray_tag
                            tasks.append(TKS.Task(cycle + " Cycle Prep, Meal " + str(meal['mealCode']) + ", Tray|" + component['title'] + " (" + trayTitle + ")",'TRAY_PORTIONING','TRAYS',meal['totalMeals']*multiplier,\
                                portion_loc,_meal = str(meal['mealCode']),_parent = meal['apiMealTitle'], _target_weight = component['qtyPounds']*GRAMS_PER_LB))
                            if(portion_day in DAYS):
                                tasks[-1].dailyCount[portion_day] += tasks[-1].totalCount
                            if(tray_tag == 'tray 1' and not tray1_sealed):
                                tasks.append(TKS.Task(cycle + " Cycle Prep, Meal " + str(meal['mealCode']) + ", Tray|" + component['title'] + " (" + trayTitle + ")",'TRAY_SEALING','TRAYS',meal['totalMeals']*multiplier,\
                                    portion_loc,_meal = str(meal['mealCode']),_parent = meal['apiMealTitle'], _target_weight = component['qtyPounds']*GRAMS_PER_LB))
                                if(portion_day in DAYS):
                                    tasks[-1].dailyCount[portion_day] += tasks[-1].totalCount
                                tray1_sealed = True
                            elif(tray_tag == 'tray 2' and not tray2_sealed):
                                tasks.append(TKS.Task(cycle + " Cycle Prep, Meal " + str(meal['mealCode']) + ", Tray|" + component['title'] + " (" + trayTitle + ")",'TRAY_SEALING','TRAYS',meal['totalMeals']*multiplier,\
                                    portion_loc,_meal = str(meal['mealCode']),_parent = meal['apiMealTitle'], _target_weight = component['qtyPounds']*GRAMS_PER_LB))
                                if(portion_day in DAYS):
                                    tasks[-1].dailyCount[portion_day] += tasks[-1].totalCount
                                tray2_sealed = True                            
                            if(_verbosity <= VL_INFORMATIONAL):
                                print("Added Tray %s on %s" % (portion_loc,portion_day))
                    except StopIteration:
                        if(_verbosity <= VL_WARNING):
                            print("Meal Component Missing Container")

        #---------------------------------------------------------------------------------------------------------
        #Compile Cooking Tasks
        #---------------------------------------------------------------------------------------------------------
        for part in parts:

            if part['combined'] == True:
                cycleTitle = "Combined"
                if(_verbosity <= VL_DEBUG):
                    print("Combined Part: %s" % part['title'])
            else:
                cycleTitle = cycle

            partWeight = part['totalWeightPounds']*totalMealCount/originalMealCount

            if(cycle != '1' and part['combined'] == True):
                continue
            elif(part['combined']):
                if cycleMethod == "PERCENTAGE_SPLIT":
                    partWeight = part['totalWeightPounds']/multiplier
                elif cycleMethod == "BY_ACTUAL_CYCLE_COUNT":
                    if(_verbosity <= VL_DEBUG):
                        print('Part Weight: %0.2f' % partWeight)
                        print('Part ID: %s' % part['id'])

                    cycle2_ID = (next(partMatch['c2ID'] for partMatch in productionRecord['cyclePartMatches'] if partMatch['id'] == part['id']))
                    if(_verbosity <= VL_DEBUG):
                        print('Part Match C2ID: %s' % cycle2_ID)

                    partWeight += (next(part['totalWeightPounds'] for part in productionRecord['cycles']['2']['parts'] if part['id'] == cycle2_ID))
                    if(_verbosity <= VL_DEBUG):
                        print('Combined Weight: %0.2f' % partWeight)

                
            categories = [tag['category'] for tag in part['tags'] if tag['category'] != 'prep_labels']
            if 'day_of_week' in categories:
                day_scheduled = next(tag['title'].capitalize() for tag in part['tags'] if tag['category'] == 'day_of_week')

                if cycleMethod == "PERCENTAGE_SPLIT":
                    task_day_offset = (next(day[0] for day in enumerate(DAYS) if day[1] == CYCLE_SHIP_DAY_MAP[cycle])-next(day[0] for day in enumerate(DAYS) if day[1] == CYCLE_SHIP_DAY_MAP["1"]))
                    day_scheduled = DAYS[(next(day[0] for day in enumerate(DAYS) if day[1] == day_scheduled)+task_day_offset) % 7]
            else:
                day_scheduled = 'Monday'

            if 'skillet' in [tag['title'] for tag in part['tags']]:
                tasks.append(TKS.Task(cycleTitle + " Cycle Prep, Skillet|" + part['title'],'SKILLET','LBS',partWeight*multiplier,'Pershing',_hours_per_batch = 3,_meal = part['apiMealCode'],_parent = part['parentPartTitle']))
                tasks[-1].dailyCount[day_scheduled] += tasks[-1].totalCount
            if 'kettle' in [tag['title'] for tag in part['tags']]:
                tasks.append(TKS.Task(cycleTitle + " Cycle Prep, Kettle|" + part['title'],'KETTLE','LBS',partWeight*multiplier,'Pershing',_hours_per_batch = 3,_meal = part['apiMealCode'],_parent = part['parentPartTitle']))
                tasks[-1].dailyCount[day_scheduled] += tasks[-1].totalCount
            if 'oven' in [tag['title'] for tag in part['tags']]:
                tasks.append(TKS.Task(cycleTitle + " Cycle Prep, Oven|" + part['title'],'OVEN','LBS',partWeight*multiplier,'Pershing',_hours_per_batch = 0.5,_meal = part['apiMealCode'],_parent = part['parentPartTitle']))
                tasks[-1].dailyCount[day_scheduled] += tasks[-1].totalCount
            if 'thaw_frozen' in [tag['title'] for tag in part['tags']]:
                tasks.append(TKS.Task(cycleTitle + " Cycle Prep, Thaw|" + part['title'],'THAW','LBS',partWeight*multiplier,'Pershing',_hours_per_batch = 2,_meal = part['apiMealCode'],_parent = part['parentPartTitle']))
                tasks[-1].dailyCount[day_scheduled] += tasks[-1].totalCount
            if 'sauce_mix' in [tag['title'] for tag in part['tags']]:
                tasks.append(TKS.Task(cycleTitle + " Cycle Prep, Sauce Mix|" + part['title'],'MIX_SAUCE','LBS',partWeight*multiplier,'Pershing',_hours_per_batch = 0.5,_meal = part['apiMealCode'],_parent = part['parentPartTitle']))
                tasks[-1].dailyCount[day_scheduled] += tasks[-1].totalCount
            if 'planetary_mixer' in [tag['title'] for tag in part['tags']]:
                tasks.append(TKS.Task(cycleTitle + " Cycle Prep, Sauce Mix|" + part['title'],'MIX_PLANETARY','LBS',partWeight*multiplier,'Pershing',_hours_per_batch = 0.5,_meal = part['apiMealCode'],_parent = part['parentPartTitle']))
                tasks[-1].dailyCount[day_scheduled] += tasks[-1].totalCount
            if 'batch_mix' in [tag['title'] for tag in part['tags']]:
                tasks.append(TKS.Task(cycleTitle + " Cycle Prep, Batch Mix|" + part['title'],'MIX_BATCH','LBS',partWeight*multiplier,'Pershing',_hours_per_batch = 0.5,_meal = part['apiMealCode'],_parent = part['parentPartTitle']))
                tasks[-1].dailyCount[day_scheduled] += tasks[-1].totalCount
            if 'knife' in [tag['title'] for tag in part['tags']]:
                tasks.append(TKS.Task(cycleTitle + " Cycle Prep, Knife|" + part['title'],'KNIFE','LBS',partWeight*multiplier,'Pershing',_hours_per_batch = 0.25,_meal = part['apiMealCode'],_parent = part['parentPartTitle']))
                tasks[-1].dailyCount[day_scheduled] += tasks[-1].totalCount
            if 'vcm' in [tag['title'] for tag in part['tags']]:
                tasks.append(TKS.Task(cycleTitle + " Cycle Prep, VCM|" + part['title'],'VCM','LBS',partWeight*multiplier,'Pershing',_hours_per_batch = 0.5,_meal = part['apiMealCode'],_parent = part['parentPartTitle']))
                tasks[-1].dailyCount[day_scheduled] += tasks[-1].totalCount
            if 'drain' in [tag['title'] for tag in part['tags']]:
                tasks.append(TKS.Task(cycleTitle + " Cycle Prep, Drain|" + part['title'],'DRAIN','LBS',partWeight*multiplier,'Pershing',_hours_per_batch = 0.5,_meal = part['apiMealCode'],_parent = part['parentPartTitle']))
                tasks[-1].dailyCount[day_scheduled] += tasks[-1].totalCount
            if 'open' in [tag['title'] for tag in part['tags']]:
                tasks.append(TKS.Task(cycleTitle + " Cycle Prep, Open|" + part['title'],'OPEN','LBS',partWeight*multiplier,'Pershing',_hours_per_batch = 0.5,_meal = part['apiMealCode'],_parent = part['parentPartTitle']))
                tasks[-1].dailyCount[day_scheduled] += tasks[-1].totalCount


    #Add Pershing Sanitation/Receiving/Dishwashing/QA
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Dishwashing",'FACILITY','EMPLOYEES',0.5+0.5*totalMealCount/50000,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Monday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Dishwashing",'FACILITY','EMPLOYEES',2+2*totalMealCount/50000,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Tuesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Dishwashing",'FACILITY','EMPLOYEES',2+2*totalMealCount/50000,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Wednesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Dishwashing",'FACILITY','EMPLOYEES',2+2*totalMealCount/50000,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Thursday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Dishwashing",'FACILITY','EMPLOYEES',1.5+1.5*totalMealCount/50000,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Friday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Dishwashing",'FACILITY','EMPLOYEES',0.5+0.5*totalMealCount/50000,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Saturday"] += tasks[-1].totalCount

    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Ingredient Collection",'FACILITY','EMPLOYEES',4.25*totalMealCount/50000,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Monday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Ingredient Collection",'FACILITY','EMPLOYEES',3*totalMealCount/50000,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Tuesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Ingredient Collection",'FACILITY','EMPLOYEES',1*totalMealCount/50000,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Wednesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Ingredient Collection",'FACILITY','EMPLOYEES',1*totalMealCount/50000,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Thursday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Ingredient Collection",'FACILITY','EMPLOYEES',1*totalMealCount/50000,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Friday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Ingredient Collection",'FACILITY','EMPLOYEES',1*totalMealCount/50000,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Sunday"] += tasks[-1].totalCount

    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Receiving",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Monday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Receiving",'FACILITY','EMPLOYEES',3,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Tuesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Receiving",'FACILITY','EMPLOYEES',2,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Wednesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Receiving",'FACILITY','EMPLOYEES',2,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Thursday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Receiving",'FACILITY','EMPLOYEES',3,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Friday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Receiving",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Saturday"] += tasks[-1].totalCount

    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Sanitation",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Monday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Sanitation",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Tuesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Sanitation",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Wednesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Sanitation",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Thursday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Sanitation",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Friday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Sanitation",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Saturday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Sanitation",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Sunday"] += tasks[-1].totalCount

    tasks.append(TKS.Task("Combined Cycle Prep, Facility|QA",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Monday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|QA",'FACILITY','EMPLOYEES',2,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Tuesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|QA",'FACILITY','EMPLOYEES',2,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Wednesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|QA",'FACILITY','EMPLOYEES',2,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Thursday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|QA",'FACILITY','EMPLOYEES',2,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Friday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|QA",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Saturday"] += tasks[-1].totalCount

    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Lead",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Monday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Lead",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Tuesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Lead",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Wednesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Lead",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Thursday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Lead",'FACILITY','EMPLOYEES',1,'Pershing',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Friday"] += tasks[-1].totalCount

    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Receiving",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Monday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Receiving",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Tuesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Receiving",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Thursday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Receiving",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Friday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Receiving",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Sunday"] += tasks[-1].totalCount

    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Sanitation",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Monday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Sanitation",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Tuesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Sanitation",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Thursday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Sanitation",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Friday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|Sanitation",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Sunday"] += tasks[-1].totalCount

    tasks.append(TKS.Task("Combined Cycle Prep, Facility|QA",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Monday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|QA",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Tuesday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|QA",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Thursday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|QA",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Friday"] += tasks[-1].totalCount
    tasks.append(TKS.Task("Combined Cycle Prep, Facility|QA",'FACILITY','EMPLOYEES',1,'Tubeway',_meal = "",_parent = ""))
    tasks[-1].dailyCount["Sunday"] += tasks[-1].totalCount

    return tasks


def compileTransportList(productionRecord, trays_per_tower = 320, meals_per_tower = 160, cups_per_tower = 1280, sachets_per_tower = 2000):

    cycles = [cycle for cycle in productionRecord['cycles']]

    pershing_to_tubeway = [] #Example object: {'item': 'ex_item_name', 'type': 'Sleeved Meal', 'qty': 2050, 'unit': 'EACH', 'transport_qty': '13', 'transported_in': 'towers', 'transport_day': 'Saturday', 'frozen': False}
    tubeway_to_pershing = [] #Example object: {'item': 'ex_item_name', 'type': 'Sleeved Meal', 'qty': 2050, 'unit': 'EACH', 'transport_qty': '13', 'transported_in': 'towers', 'transport_day': 'Saturday', 'frozen': False}

    for cycle in cycles:

        meals = productionRecord['cycles'][cycle]['meals'] #Use the meals (and counts) for each cycle

        for meal in meals:

            tray_1_acctd_for = False
            tray_2_acctd_for = False

            if(str(cycle) == "2"):
                transport_day = 'Monday2'
            else:
                transport_day = 'Saturday'

            if 'pershing' in next(tag['title'] for tag in meal['tags'] if tag['category'] == "sleeving_location"):
                item_name = "Sleeved Meal " + str(meal['mealCode']) + " (Cycle " + str(cycle) + ")"
                pershing_to_tubeway.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'Sleeved Meal', 'qty': meal['totalMeals'], 'unit': 'EACH', 'transport_qty': np.ceil(meal['totalMeals']/meals_per_tower),\
                    'transported_in': 'Towers', 'transport_day': transport_day, 'frozen': False})


                for component in meal['billOfMaterials']:
                    try: 
                        portion_day = next(tag['title'] for tag in component['tags'] if tag['category'] == 'portion_date').capitalize()
                    except StopIteration:
                        portion_day = 'Monday'
                    try:
                        portion_loc = next(tag['title'] for tag in component['tags'] if tag['category'] == 'portion_location').capitalize()
                    except StopIteration:
                        portion_loc = 'Pershing'
                    try:
                        tray_tag = next(tag['title'] for tag in component['tags'] if tag['category'] == 'container')
                    except StopIteration:
                        tray_tag = ''
                    try:
                        fc = next(tag['title'] for tag in component['tags'] if tag['category'] == 'frozen')
                    except StopIteration:
                        fc = 'no'

                    if(portion_loc == 'Tubeway'):

                        try:
                            transport_day = DAYS[(next(day[0] for day in enumerate(DAYS) if day[1] == portion_day)+1)%7]
                        except StopIteration:
                            transport_day = 'Monday2'

                        if(('tray 1' in tray_tag) and not tray_1_acctd_for):
                            if("bag" in tray_tag):
                                tray_1_acctd_for = True
                                item_name = str(component['title']) + " (Meal " + str(meal['mealCode']) +", Cycle " + str(cycle) + ")"
                            
                                tubeway_to_pershing.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'BAGS', 'qty': meal['totalMeals'], 'unit': 'EACH', 'transport_qty': np.ceil(meal['totalMeals']/trays_per_tower),\
                                    'transported_in': 'Towers', 'transport_day': transport_day, 'frozen': fc == 'yes'})
                        if(('tray 2' in tray_tag) and not tray_2_acctd_for):
                            if("bag" in tray_tag):
                                tray_2_acctd_for = True
                                item_name = str(component['title']) + " (Meal " + str(meal['mealCode']) +", Cycle " + str(cycle) + ")"
                            
                                tubeway_to_pershing.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'BAGS', 'qty': meal['totalMeals'], 'unit': 'EACH', 'transport_qty': np.ceil(meal['totalMeals']/trays_per_tower),\
                                    'transported_in': 'Towers', 'transport_day': transport_day, 'frozen': fc == 'yes'})
                        if(tray_tag == "sachet"):
                            item_name = str(component['title']) + " (Meal " + str(meal['mealCode']) +", Cycle " + str(cycle) + ")"

                            pershing_to_tubeway.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'Bulk', 'qty': component['totalWeightRequiredPounds'], 'unit': 'LBS', 'transport_qty': "",\
                                'transported_in': 'Drums', 'transport_day': 'Wednesday', 'frozen': fc == 'yes'})
                            
                            tubeway_to_pershing.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'Sachets', 'qty': meal['totalMeals'], 'unit': 'EACH', 'transport_qty': np.ceil(meal['totalMeals']/sachets_per_tower),\
                                'transported_in': 'Towers', 'transport_day': transport_day, 'frozen': fc == 'yes'})

            elif 'tubeway' in next(tag['title'] for tag in meal['tags'] if tag['category'] == "sleeving_location"):
                for component in meal['billOfMaterials']:
                    try: 
                        portion_day = next(tag['title'] for tag in component['tags'] if tag['category'] == 'portion_date').capitalize()
                    except StopIteration:
                        portion_day = 'Monday'
                    try:
                        portion_loc = next(tag['title'] for tag in component['tags'] if tag['category'] == 'portion_location').capitalize()
                    except StopIteration:
                        portion_loc = 'Pershing'
                    try:
                        tray_tag = next(tag['title'] for tag in component['tags'] if tag['category'] == 'container')
                    except StopIteration:
                        tray_tag = ''
                    try:
                        fc = next(tag['title'] for tag in component['tags'] if tag['category'] == 'frozen')
                    except StopIteration:
                        fc = 'no'

                    if(portion_loc == 'Tubeway'):

                        try:
                            part = next(part for part in productionRecord['cycles'][cycle]['parts'] if component['productionPartID'] == part['id'])
                            try:
                                prep_day = next(tag['title'] for tag in part['tags'] if tag['category'] == 'day_of_week').capitalize()
                            except StopIteration:
                                prep_day = 'Monday'
                        except StopIteration:
                            prep_day = 'Monday'

                        item_name = str(component['title']) + " (Meal " + str(meal['mealCode']) +", Cycle " + str(cycle) + ")"

                        if(("bag" in tray_tag) or ("vac pack" in tray_tag)):
                            continue

                        if(tray_tag == "pre-portioned"):
                            item_name = str(component['title']) + " (Meal " + str(meal['mealCode']) +", Cycle " + str(cycle) + ")"
                            
                            pershing_to_tubeway.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'Pre-Portioned', 'qty': meal['totalMeals'], 'unit': 'EACH', 'transport_qty': "",\
                                'transported_in': 'CASES', 'transport_day': transport_day, 'frozen': fc == 'yes'})
                            continue

                        if(tray_tag == "sachet"):
                            item_name = str(component['title']) + " (Meal " + str(meal['mealCode']) +", Cycle " + str(cycle) + ")"
                            
                            pershing_to_tubeway.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'Bulk', 'qty': component['totalWeightRequiredPounds'], 'unit': 'LBS', 'transport_qty': "",\
                                'transported_in': 'Drums', 'transport_day': 'Wednesday', 'frozen': fc == 'yes'})
                            continue
                            
                        pershing_to_tubeway.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'Bulk', 'qty': component['totalWeightRequiredPounds'], 'unit': 'LBS', 'transport_qty': "",\
                            'transported_in': 'Cases', 'transport_day': 'Wednesday', 'frozen': fc == 'yes'})

                    else:
                        try:
                            transport_day = DAYS[(next(day[0] for day in enumerate(DAYS) if day[1] == portion_day)+1)%7]
                        except StopIteration:
                            transport_day = 'Monday2'

                        if(('tray 1' in tray_tag) and not tray_1_acctd_for):
                            if(("bag" in tray_tag) or ("clamshell" in tray_tag) or ("vac pack" in tray_tag)):
                                print("WARNING: Bags, clamshells, and vac pack are currently not being included on the transport list from Pershing to Tubeway")
                                continue
                            tray_1_acctd_for = True
                            item_name = str(component['title']) + " (Meal " + str(meal['mealCode']) +", Cycle " + str(cycle) + ")"
                            
                            pershing_to_tubeway.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'Trays', 'qty': meal['totalMeals'], 'unit': 'EACH', 'transport_qty': np.ceil(meal['totalMeals']/trays_per_tower),\
                                'transported_in': 'Towers', 'transport_day': transport_day, 'frozen': fc == 'yes'})

                        if(('tray 2' in tray_tag) and not tray_2_acctd_for):
                            if(("bag" in tray_tag) or ("clamshell" in tray_tag) or ("vac pack" in tray_tag)):
                                print("WARNING: Bags, clamshells, and vac pack are currently not being included on the transport list from Pershing to Tubeway")
                                continue
                            tray_2_acctd_for = True
                            item_name = str(component['title']) + " (Meal " + str(meal['mealCode']) +", Cycle " + str(cycle) + ")"
                            
                            pershing_to_tubeway.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'Trays', 'qty': meal['totalMeals'], 'unit': 'EACH', 'transport_qty': np.ceil(meal['totalMeals']/trays_per_tower),\
                                'transported_in': 'Towers', 'transport_day': transport_day, 'frozen': fc == 'yes'})

                        if((tray_tag == "2 oz cup") or (tray_tag == "1 oz cup")):
                            item_name = str(component['title']) + " (Meal " + str(meal['mealCode']) +", Cycle " + str(cycle) + ")"
                            
                            pershing_to_tubeway.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'Cups', 'qty': meal['totalMeals'], 'unit': 'EACH', 'transport_qty': np.ceil(meal['totalMeals']/cups_per_tower),\
                                'transported_in': 'Towers', 'transport_day': transport_day, 'frozen': fc == 'yes'})

                        if(tray_tag == "sachet"):
                            item_name = str(component['title']) + " (Meal " + str(meal['mealCode']) +", Cycle " + str(cycle) + ")"
                            
                            pershing_to_tubeway.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'Sachets', 'qty': meal['totalMeals'], 'unit': 'EACH', 'transport_qty': np.ceil(meal['totalMeals']/sachets_per_tower),\
                                'transported_in': 'Towers', 'transport_day': transport_day, 'frozen': fc == 'yes'})

                        if(tray_tag == "pre-portioned"):
                            item_name = str(component['title']) + " (Meal " + str(meal['mealCode']) +", Cycle " + str(cycle) + ")"
                            
                            pershing_to_tubeway.append({'cycle': cycle, 'meal': meal['mealCode'], 'item': item_name,'type': 'Pre-Portioned', 'qty': meal['totalMeals'], 'unit': 'EACH', 'transport_qty': "",\
                                'transported_in': 'CASES', 'transport_day': transport_day, 'frozen': fc == 'yes'})
        
    return [pershing_to_tubeway,tubeway_to_pershing]


##PUBLIC <<DEPRECATED>>
#This function generates a Google sheet for assisting with labor planning by taking assigned tasks and dropping them on to an overlay of the facility.
#While this view may eventually be useful, it is a poor way of currently communicating utilization, and is not well executed in Google sheets. 
def generateTaskListSpreadsheet(_facility_network, productionRecord, tasks, service, spreadsheet_id):

    vals = []
    tubewayRows = []
    counter = 1

    for facility in _facility_network.facilityList:

        counters = {'TRAY_PORTIONING': 0,'GARNISH_PORTIONING': 0,'SLEEVING':0}

        vals = []
        dataValReqs = []
        sheetId = gs.getSheetIDFromName(facility.name + " Task List",service,spreadsheet_id)
        counter = 2

        for task in tasks:

            if task.location == facility.name:
                task_equipment_options = [equipment for equipment in facility.equipment_list if task.process in [process.name for process in equipment['equipment'].processList]]

                [task,counters] = scheduler.equipment_scheduler(task, task_equipment_options,_facility_network,counters)

                eqList = []
                rate_per_hour_str = ["=","","","",""]
                lbs_per_batch_str = ["=","","","",""]
                hours_per_batch_str = "="

                workersStr = ["=","","","",""]
                line_hours_str = "="
                eqCount = 0
                for equip in task_equipment_options:

                    eqList.append(equip['name'])

                    eq_process = next(process for process in equip['equipment'].processList if process.name == task.process)
                    if(isinstance(eq_process,eq.ProcessObject)):
                        rate_per_hour_str[0] += "IF("+GSHEETS_COLUMN_MAP['Eq 1']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.rate_per_hour)+","
                        rate_per_hour_str[1] += "IF("+GSHEETS_COLUMN_MAP['Eq 2']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.rate_per_hour)+","
                        rate_per_hour_str[2] += "IF("+GSHEETS_COLUMN_MAP['Eq 3']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.rate_per_hour)+","
                        rate_per_hour_str[3] += "IF("+GSHEETS_COLUMN_MAP['Eq 4']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.rate_per_hour)+","
                        rate_per_hour_str[4] += "IF("+GSHEETS_COLUMN_MAP['Eq 5']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.rate_per_hour)+","

                        lbs_per_batch_str[0] += "IF("+GSHEETS_COLUMN_MAP['Eq 1']+str(counter)+"=\""+equip['name']+"\",\"\","
                        lbs_per_batch_str[1] += "IF("+GSHEETS_COLUMN_MAP['Eq 2']+str(counter)+"=\""+equip['name']+"\",\"\","
                        lbs_per_batch_str[2] += "IF("+GSHEETS_COLUMN_MAP['Eq 3']+str(counter)+"=\""+equip['name']+"\",\"\","
                        lbs_per_batch_str[3] += "IF("+GSHEETS_COLUMN_MAP['Eq 4']+str(counter)+"=\""+equip['name']+"\",\"\","
                        lbs_per_batch_str[4] += "IF("+GSHEETS_COLUMN_MAP['Eq 5']+str(counter)+"=\""+equip['name']+"\",\"\","

                        hours_per_batch_str += "IF("+GSHEETS_COLUMN_MAP['Eq 1']+str(counter)+"=\""+equip['name']+"\",\"\","

                        line_hours_str += "IF("+GSHEETS_COLUMN_MAP['Eq 1']+str(counter)+"=\""+equip['name']+"\","+GSHEETS_COLUMN_MAP['Ct']+str(counter)+"/"+GSHEETS_COLUMN_MAP['RpH']\
                            +str(counter)+"+"+str(eq_process.changeover_time)+","

                    elif(isinstance(eq_process,eq.BatchProcessObject)):
                        rate_per_hour_str[0] += "IF("+GSHEETS_COLUMN_MAP['Eq 1']+str(counter)+"=\""+equip['name']+"\",\"\","
                        rate_per_hour_str[1] += "IF("+GSHEETS_COLUMN_MAP['Eq 2']+str(counter)+"=\""+equip['name']+"\",\"\","
                        rate_per_hour_str[2] += "IF("+GSHEETS_COLUMN_MAP['Eq 3']+str(counter)+"=\""+equip['name']+"\",\"\","
                        rate_per_hour_str[3] += "IF("+GSHEETS_COLUMN_MAP['Eq 4']+str(counter)+"=\""+equip['name']+"\",\"\","
                        rate_per_hour_str[4] += "IF("+GSHEETS_COLUMN_MAP['Eq 5']+str(counter)+"=\""+equip['name']+"\",\"\","

                        lbs_per_batch_str[0] += "IF("+GSHEETS_COLUMN_MAP['Eq 1']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.max_weight_per_batch)+","
                        lbs_per_batch_str[1] += "IF("+GSHEETS_COLUMN_MAP['Eq 2']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.max_weight_per_batch)+","
                        lbs_per_batch_str[2] += "IF("+GSHEETS_COLUMN_MAP['Eq 3']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.max_weight_per_batch)+","
                        lbs_per_batch_str[3] += "IF("+GSHEETS_COLUMN_MAP['Eq 4']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.max_weight_per_batch)+","
                        lbs_per_batch_str[4] += "IF("+GSHEETS_COLUMN_MAP['Eq 5']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.max_weight_per_batch)+","

                        hours_per_batch_str += "IF("+GSHEETS_COLUMN_MAP['Eq 1']+str(counter)+"=\""+equip['name']+"\","+str(task.hours_per_batch)+","

                        line_hours_str += "IF("+GSHEETS_COLUMN_MAP['Eq 1']+str(counter)+"=\""+equip['name']+"\",CEILING("+GSHEETS_COLUMN_MAP['Ct']+str(counter)+"/"+GSHEETS_COLUMN_MAP['LpB']+str(counter)+")\
                            *"+GSHEETS_COLUMN_MAP['HpB']+str(counter)+"+"+str(eq_process.changeover_time)+","

                    else:
                        print("Unrecognized Process Type Found for %s" % eq_process.name)

                    workersStr[0] += "IF("+GSHEETS_COLUMN_MAP['Eq 1']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.N_workers)+","
                    workersStr[1] += "IF("+GSHEETS_COLUMN_MAP['Eq 2']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.N_workers)+","
                    workersStr[2] += "IF("+GSHEETS_COLUMN_MAP['Eq 3']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.N_workers)+","
                    workersStr[3] += "IF("+GSHEETS_COLUMN_MAP['Eq 4']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.N_workers)+","
                    workersStr[4] += "IF("+GSHEETS_COLUMN_MAP['Eq 5']+str(counter)+"=\""+equip['name']+"\","+str(eq_process.N_workers)+","

                    eqCount += 1

                for i in range(5):
                    rate_per_hour_str[i] += "0"
                    lbs_per_batch_str[i] += "0"
                    workersStr[i] += "0"
                hours_per_batch_str += "0"
                line_hours_str += "0"
                for i in range(eqCount):
                    for j in range(5):
                        rate_per_hour_str[j] += ")"
                        lbs_per_batch_str[j] += ")"
                        workersStr[j] += ")"
                    hours_per_batch_str += ")"
                    line_hours_str += ")"

                _rate_per_hour_str = rate_per_hour_str[0]+"+"+rate_per_hour_str[1]+"+"+rate_per_hour_str[2]+"+"+rate_per_hour_str[3]+"+"+rate_per_hour_str[4]
                _lbs_per_batch_str = lbs_per_batch_str[0]+"+"+lbs_per_batch_str[1]+"+"+lbs_per_batch_str[2]+"+"+lbs_per_batch_str[3]+"+"+lbs_per_batch_str[4]
                _workersStr = workersStr[0]+"+"+workersStr[1]+"+"+workersStr[2]+"+"+workersStr[3]+"+"+workersStr[4]

                dataValReqs = gs.compileDataValidationCells(sheetId, dataValReqs, counter-1, 12, 17, service, spreadsheet_id, eqList)

                try:
                    task_type = TASK_TYPE_MAP[task.process]
                except KeyError:
                    task_type = task.process
                try:
                    task_day = next(day for day in task.dailyCount if task.dailyCount[day]>0)
                    task_date = str(scheduler.next_weekday(datetime.date.today(),DATETIME_DAY_MAP[task_day]))

                except StopIteration:
                    task_day = ""
                    task_date = ""
                if ((task.target_weight > 0) and (task.process == "TRAY_PORTIONING" or task.process == "GARNISH_CUP_PORTIONING_SEALING" or task.process == "SACHET_DEPOSITING")):
                    lower_weight = task.target_weight - NIST_MAV(task.target_weight)
                    upper_weight = task.target_weight + NIST_MAV(task.target_weight)
                else:
                    lower_weight = ""
                    upper_weight = ""

                fullnameStr = "=textjoin(\"\",TRUE,"+GSHEETS_COLUMN_MAP['TskNm']+str(counter)+",char(10),\"(Meal \","+GSHEETS_COLUMN_MAP['Meal']+str(counter)+",\", Cycle \","+GSHEETS_COLUMN_MAP['Cycle']+str(counter)+",\")\")"
                defaultPriority = "10"
                
                vals.extend([[task.name.split("Cycle")[0],task.meal,task.parent,task.name.split("|")[-1],fullnameStr,task_type,task.totalCount,task.output_unit,\
                    lower_weight,upper_weight,"","",task.assigned_equipment[0],\
                    task.assigned_equipment[1],task.assigned_equipment[2],task.assigned_equipment[3],task.assigned_equipment[4],"",_rate_per_hour_str,_lbs_per_batch_str,hours_per_batch_str,\
                    _workersStr,"","","","="+GSHEETS_COLUMN_MAP['QP']+str(counter)+"-"+GSHEETS_COLUMN_MAP['Ct']+str(counter),line_hours_str,"="+GSHEETS_COLUMN_MAP['wrks']+str(counter)+"*"+GSHEETS_COLUMN_MAP['LH']+str(counter),task_day,task_date,defaultPriority]])
                
                counter += 1

        
        gs.batchUpdate(dataValReqs, service, spreadsheet_id)
        gs.writeValues(facility.name + " Task List!A2:"+GSHEETS_COLUMN_MAP['DefPri']+"1000",vals,service,spreadsheet_id)
        vals = []
        eqSummary = ["","","","","","",""]
        for equipment in facility.equipment_list:
            for day in enumerate(DAYS):
                eqSummary[day[0]] = "=SUMIFS('" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['LH']+"2:"+GSHEETS_COLUMN_MAP['LH']+"1000,'" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['Eq 1']+"2:"+GSHEETS_COLUMN_MAP['Eq 1']+"1000,\"=" + equipment['name'] + "\",'" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['DA']+"2:"+GSHEETS_COLUMN_MAP['DA']+"1000,\"=" + day[1] + "\")\
                                    +SUMIFS('" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['LH']+"2:"+GSHEETS_COLUMN_MAP['LH']+"1000,'" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['Eq 2']+"2:"+GSHEETS_COLUMN_MAP['Eq 2']+"1000,\"=" + equipment['name'] + "\",'" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['DA']+"2:"+GSHEETS_COLUMN_MAP['DA']+"1000,\"=" + day[1] + "\")\
                                    +SUMIFS('" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['LH']+"2:"+GSHEETS_COLUMN_MAP['LH']+"1000,'" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['Eq 3']+"2:"+GSHEETS_COLUMN_MAP['Eq 3']+"1000,\"=" + equipment['name'] + "\",'" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['DA']+"2:"+GSHEETS_COLUMN_MAP['DA']+"1000,\"=" + day[1] + "\")\
                                    +SUMIFS('" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['LH']+"2:"+GSHEETS_COLUMN_MAP['LH']+"1000,'" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['Eq 4']+"2:"+GSHEETS_COLUMN_MAP['Eq 4']+"1000,\"=" + equipment['name'] + "\",'" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['DA']+"2:"+GSHEETS_COLUMN_MAP['DA']+"1000,\"=" + day[1] + "\")\
                                    +SUMIFS('" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['LH']+"2:"+GSHEETS_COLUMN_MAP['LH']+"1000,'" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['Eq 5']+"2:"+GSHEETS_COLUMN_MAP['Eq 5']+"1000,\"=" + equipment['name'] + "\",'" + facility.name + " Task List'!"+GSHEETS_COLUMN_MAP['DA']+"2:"+GSHEETS_COLUMN_MAP['DA']+"1000,\"=" + day[1] + "\")"

            vals.extend([[equipment['name'],equipment['equipment'].type,eqSummary[0],eqSummary[1],eqSummary[2],eqSummary[3],eqSummary[4],eqSummary[5],eqSummary[6]]])
        gs.writeValues(facility.name + " Equipment Summary!A2:I1000",vals,service,spreadsheet_id)