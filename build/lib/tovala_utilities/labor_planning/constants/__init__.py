from __future__ import division
import numpy as np
import math
import datetime
#from .. import data_import as datIn

PRODUCTION_RECORD_URL = "https://misevala-api.tvla.co/v0/productionRecords/"
DEV_PRODUCTION_RECORD_URL = "https://misevala-api.dev.tvla.co/v0/productionRecords/"

GRAMS_PER_LB = 453.592

#Debug Print (Verbosity) Levels
VL_INFORMATIONAL = 1
VL_DEBUG = 2
VL_WARNING = 3
VL_ERROR = 4

#Global Assumptions
N_MEALS_PER_BOX_AVG = 5.6

#BOXES/ICE
#At 81 k meals, we will be using 2200x 3 meal boxes, 6000x 4 meal boxes, 6000x 6 meal boxes, 1800x 8 meal boxes
MEAL_CT_75K = 75000
N_3MEAL_BOXES_75K = 2200*75/81
N_4MEAL_BOXES_75K = 6000*75/81
N_6MEAL_BOXES_75K = 6000*75/81
N_8MEAL_BOXES_75K = 1800*75/81
FRAC_SLC = 0.28
FRAC_LEW = 0.22
FRAC_OLB = 0.14
FRAC_UDS = 0.12
FRAC_CHI = 0.24
FRAC_1ST_PACKOUT_DAY = FRAC_SLC+FRAC_LEW+FRAC_OLB+FRAC_UDS
FRAC_2ND_PACKOUT_DAY = FRAC_CHI
DAYS = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
ZEROS_BY_DAY={'Sunday': 0, 'Monday': 0, 'Tuesday': 0, 'Wednesday': 0, 'Thursday': 0, 'Friday': 0, 'Saturday': 0}
DEFAULT_COLOR = 'Blue'
DEFAULT_COLOR_BY_DAY = {'Sunday': 'Blue', 'Monday': 'Blue', 'Tuesday': 'Blue', 'Wednesday': 'Blue', 'Thursday': 'Blue', 'Friday': 'Blue', 'Saturday': 'Blue'}


validProcesses = [
    'TRAY_PORTIONING',
    'TRAY_SEALING',
    'BAND_SEALING',
    'SACHET_DEPOSITING',
    'GARNISH_CUP_PORTIONING_SEALING',
    'SLEEVING',
    'PACKOUT',
    'SNAPPING',
    'SKILLET',
    'KETTLE',
    'OVEN',
    'THAW',
    'FREEZE',
    'MIX_SAUCE',
    'MIX_BATCH',
    'MIX_PLANETARY',
    'KNIFE',
    'ROBO_COUPE',
    'VCM',
    'DRAIN',
    'OPEN',
    'FACILITY',
    'OVEN_MEATBALL',
    'FORMING_MEATBALL'
]

validOutputUnits = [
    'SACHETS',
    '1_OZ_CUPS',
    '2_OZ_CUPS',
    'TRAYS',
    'BAGS',
    'CLAMSHELLS',
    'MEALS',
    'BOXES',
    'LBS',
    'EACH',
    'EMPLOYEES'
]

ALLOWABLE_CYCLE_METHODS = [
    'PERCENTAGE_SPLIT',
    'BY_ACTUAL_CYCLE_COUNT'
]

CYCLE_SHIP_DAY_MAP = {
    "1": 'Sunday',
    "2": 'Tuesday'
}

TASK_TYPE_MAP = {
    'PACKOUT': 'Packout',
    'SLEEVING': 'Sleeving',
    'BAND_SEALING': 'Band Sealing',
    'SACHET_DEPOSITING': 'Sachet',
    'TRAY_SEALING': 'Sealing',
    'GARNISH_CUP_PORTIONING_SEALING': 'Portioning',
    'TRAY_PORTIONING': 'Portioning'
}

DATETIME_DAY_MAP = {
    'Monday': 0,
    'Tuesday': 1,
    'Wednesday': 2,
    'Thursday': 3,
    'Friday': 4,
    'Saturday': 5,
    'Sunday': 6,
    'Monday2': 7
}

GSHEETS_COLUMN_MAP = {  #FOR LABOR PLANNING SHEET
    'Eq 1': 'M',#'H', #Equipment Selected 1
    'Eq 2': 'N',#'I',
    'Eq 3': 'O',#'J',
    'Eq 4': 'P',#'K',
    'Eq 5': 'Q',#'L',
    'Meal': 'B', #Meal Code
    'Cycle': 'A', #Cycle
    'TskNm': 'D', #Task Name
    'FullNm': 'E', #Full Name
    'Ct': 'G', #Count
    'RpH': 'S',#"M", #Rate Per Hour
    'LpB': 'T',#'N', #Pounds per Batch
    'HpB': 'U',#'O', #Hours per Batch
    'wrks': 'V',#'P', #Workers
    'LH': 'AA',#'Q', #Line Hours
    'DA': 'AC',#'S' #Day Allocated
    'DatA': 'AD',#'S' #Date Allocated
    'QP': 'Y',#'S' #Qty Produced
    'DefPri': 'AE' #Default Priority 
}