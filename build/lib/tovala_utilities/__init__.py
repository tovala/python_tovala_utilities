from . import curve_fit as cf
from . import data_import as datIn
from . import electromagnetics as em
from . import intersection as intersect
from . import math_utility as mu
from . import meal_parsing as mP
from . import steam as stm
from . import temperature as tlb
from . import time_manipulation as tman

def mapTimeArray(tFrom, data, tTo,percentFlag = 1):
	[dataMap,binCount] = tman.mapTimeArray(tFrom, data, tTo,percentFlag = 1)
	return [dataMap,binCount]

def prepArray(_array):
	array = mu.prepArray(_array)
	return array

def importArbitraryCSV(filename,headers = 0):
	[_index,_data] = datIn.importArbitraryCSV(filename,headers = 0)
	return [_index,_data]

def expDecayCurveFit(_time,_signal,x0):
	[fitVar,fitSig] = cf.expDecayCurveFit(_time,_signal,x0)
	return [fitVar,fitSig]