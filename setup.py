import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="tovala_utilities",
    version="0.3.2",
    author="Peter Fiflis",
    author_email="peter@tovala.com",
    description="Module that provides a number of Tovala utility functions. May be readily installed for access to commonly used Tovala functions",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/tovala/python_tovala_utilities",
    packages=setuptools.find_packages(exclude=['test*']),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    include_package_data=True,
    install_requires=[
          'numpy',
          'scipy',
          'urllib3',
          'datetime',
          'matplotlib'
    ]
)