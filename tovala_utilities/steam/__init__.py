# -*- coding: utf-8 -*-
"""
Created on Wed May 24 11:18:51 2017

@author: Peter
"""
#Python Imports
from __future__ import division
import csv
import numpy as np
import matplotlib.pylab as plt
import scipy.optimize as optimization
import scipy.signal as signalproc
import math
import urllib3 as url
import json
import datetime
from .. import math_utility as mu
from .. import temperature as tlb
from .. import data_import as datIn
from .. import curve_fit as cf


#Sub-Module Constants
savgolFilterWindow = 3001 #Picked To Be Number of Samples Recorded in One Minute (50 samples/sec * 60 sec/min) + 1

def vaporPressure(_T):   #Utility function to calculate the vapor pressure of water (in Pascals) given a temperature (in Celsius)
    vP = 101325/760*np.exp(math.log(10)*(8.07131-1730.63/(233.426+_T))) #Source: Simplification of the Clausius-Clapeyron equation
    return vP
    
def RH_to_PP(_temp,_rh):  #Utility function for calculating the partial pressure of water (in Pascals) given a temperature (in Celsius) and a relative humidity (fraction between 0 and 1)
    if(len(_temp) != len(_rh)):  #Check to ensure that the input arrays are the same length
        raise IndexError("Temp and RH Arrays Must Be Same Length") 
    vP = vaporPressure(_temp)
    pP = _rh*vP  #From definition of relative humidity
    return pP

def PP_to_RH(_temp,pP):  #Inverse of the RH_to_PP function
    if(len(_temp) != len(pP)):
        raise IndexError("Temp and Partial Pressure Arrays Must Be Same Length")
    vP = vaporPressure(_temp)
    rh = pP/vP
    return rh

def sortDBWBTemps(T1,T2,T3,T4):   #Utility function that sorts a set of four temperature traces by their averages (in descending order)
    maxAve = [np.max(np.average(T1)),np.max(np.average(T2)),np.max(np.average(T3)),np.max(np.average(T4))]
    T = [T1,T2,T3,T4]
    indices = np.argsort(maxAve)

    return [T[indices[3]],T[indices[2]],T[indices[1]],T[indices[0]]]

def importHX15_Ddata(filename,rhChannel=0,TChannel=1): #2 Channel Data Import to pull in date from the HX-15D relative humidity probe. Comments Tagged for Frankenvala RH DAQ Setup

    [time,_dataNP] = datIn.import_MCDAQ_CSV(filename,headers = 7)

    RH=_dataNP[:,rhChannel];         #Relative Humidity
    T=_dataNP[:,TChannel];          #Temperature
            
    time=time/60;

    return [time,RH,T]

def importDBWB_RTD_Ddata(filename,lengthofAvg,RTDChannels=[0,1,2,3]): #4 Channel Data Import to pull in date from the Tovala DBWB sensor. Comments Tagged for Frankenvala Wet Bulb/Dry Bulb DAQ Setup
    if (len(RTDChannels)!=4):
        raise IndexError("Index Array for RTDs Must Be len = 4")

    [time,_dataNP] = datIn.import_MCDAQ_CSV(filename,headers = 7)

    T1=_dataNP[:,RTDChannels[0]];         #Temperature 1
    T2=_dataNP[:,RTDChannels[1]];         #Temperature 2
    T3=_dataNP[:,RTDChannels[2]];         #Temperature 3
    T4=_dataNP[:,RTDChannels[3]];         #Temperature 4

    T1=tlb.RTDvoltsToTempC(tlb.prepData(T1,lengthofAvg))
    T2=tlb.RTDvoltsToTempC(tlb.prepData(T2,lengthofAvg))
    T3=tlb.RTDvoltsToTempC(tlb.prepData(T3,lengthofAvg))
    T4=tlb.RTDvoltsToTempC(tlb.prepData(T4,lengthofAvg))
    [T_DB1,T_DB2,T_WB1,T_WB2] = sortDBWBTemps(T1,T2,T3,T4)

    time=time/60;
    time=time[0:len(time)-lengthofAvg]

    return [time,T_DB1,T_DB2,T_WB1,T_WB2]

def orderDecayIndices(indexMax,indexMin):  #Utility function to ensure that decays are fit on a portion of a trace starting with a local maxima and ending with a local minima

    #print(np.shape(indexMax))
    #print(np.shape(indexMin))

    if(len(indexMax)>len(indexMin)):  #If there are more maxima then minima, discard the final maxima. (Works because local maxima must always occur between local minima and vice versa)
        indexMax = indexMax[0:len(indexMax)-2]
    elif(len(indexMax)<len(indexMin)): #If there are more minima thne maxima, discard the first minima.
        indexMin = indexMin[1,len(indexMax)-1]
    else:
        if(indexMax[0]>indexMin[0]):  #If the first maxima is after the first minima, discard both the first minima and the final maxima.
            indexMax = indexMax[0:len(indexMax)-2]
            indexMin = indexMin[1:len(indexMin)-1]
    return [indexMax,indexMin]

def fitDecays(time,trace,fitRises=0,ignoreThreshold=0,steamOnTime=0.5,steamCycleTime = 10,plot=0,plotRise=0):  
    #Main function for fitting the decays of an exponential signal (in this case, relative humidty)
    #Takes in as inputs a time and an RH trace as well as several flags:
        #fitRises: If 0, only fit the decays of the RH signal, if 1, also fit the increases in the RH signal with the fitRises() function
        #ignoreThreshold: Sets the RH difference (between maxima and minima on a fit) below which the decay will be ignored (set at 5% for experiments in early 2018)
        #steamOnTime and steamCycleTime: Both variables needed for the fitRises() function and will be discussed there instead of here
        #plot: Flag to include plotting of the fits against the data. 0 = don't plot, 1 = plot.
        #plotRise: Sets a similar flag to plot for the fitRises() function

    indicesMax = np.transpose(mu.detectLocalMaxima(trace))   #Find the maxima and minima of the trace
    indicesMin = np.transpose(mu.detectLocalMinima(trace))

    [indicesMax,indicesMin] = orderDecayIndices(indicesMax,indicesMin) #Order so that a decay fit is had between a maxima and a minima

    if(plot!=0):  #Set up plot to show decay fits against original signal (useful for checking validity of fits)
        fig100=plt.figure()
        plt.title("Decay Time Constant Fitting")
        plt.xlabel("Time (min)")
        plt.ylabel("Signal (A.U.)")
        #plt.axis("OFF")
        ax100=fig100.add_subplot(1,1,1) 
        ax100.plot(time,trace, 'r')

    decayConst = 0
    fitCount = 0

    indexQueue = []
    stEC = []
    stI = []

    for i in range(1,len(indicesMax)):
        if((indicesMin[i]-indicesMax[i])>1000):
            [fitVar,fitSig] = cf.expDecayCurveFit(time[indicesMax[i]:indicesMin[i]],trace[indicesMax[i]:indicesMin[i]])  #Fit the trace portion between each maxima and minima
            if(plot!=0):
                ax100.plot(time[indicesMax[i]:indicesMin[i]],fitSig,'b--')  #Plot the fit if desired.
            if(fitVar[0]>ignoreThreshold):        #Decays in RH from a maximum value of ignoreThreshold or less are regarded as small amounts of steam drips and therefore ignored
                decayConst = (decayConst*fitCount + fitVar[1])/(fitCount+1)
                fitCount = fitCount + 1
                if(fitRises):
                    indexQueue.append(indicesMax[i])

    if(fitRises):
        [stEC,stI]=fitRise(time,trace,indexQueue,decayConst,steamOnTime,steamCycleTime,plotRise)
    
    return [decayConst,stEC,stI]

def fitPulsedSteam(_time,_signal,decayConst,_steamCycleTime,ignoreThreshold=0,plot=0):

    _signal = signalproc.savgol_filter(_signal,301,2)

    if(plot!=0): #Set up plot if desired to show fits
        fig101=plt.figure()
        plt.title("Steam Injection Time Constant Fit")
        plt.xlabel("Time")
        plt.ylabel("Signal (A.U.)")
        #plt.axis("OFF")
        ax101=fig101.add_subplot(1,1,1) 
        ax101.plot(_time,_signal, 'r')
        ax101.plot(_time[0:len(_time)-1],3000*np.diff(_signal), 'b')
        ax101.plot(_time[0:len(_time)-2],300*3000*signalproc.savgol_filter(np.diff(signalproc.savgol_filter(np.diff(_signal),301,2)),301,2), 'g')

    indicesMax = mu.find2ndDerivLocalMaxima(_signal)  #Steam injections (and subsequent decays) are bounded by maxima in the 2nd derivative of the humidity trace on ovens with steam injection time constants much shorter than their steam decay time constants

    steamEntryConstant = 0
    steamInput = 0
    fitCount = 0
    
    for i in range(0,len(indicesMax)-1):
        #ax101.plot([_time[indicesMax[i]],_time[indicesMax[i]]],[0,20], 'k')
        endIndex = np.argmin(np.abs(_time-(_time[indicesMax[i]]+_steamCycleTime)))
        [fitVar,fitSig] = cf.chainDecayCurveFit(_time[indicesMax[i]:endIndex],_signal[indicesMax[i]:endIndex])
        if(plot!=0):
            ax101.plot(_time[indicesMax[i]:endIndex],fitSig,'b--')
            #ax101.plot([_time[endIndex],_time[endIndex]],[0,20], 'k--')
        steamInputI = fitVar[2]*(decayConst-fitVar[0])/fitVar[0]
        if(fitVar[0]>ignoreThreshold):        #Decays in steam entry from a maximum value of ignoreThreshold or less are regarded as "Unable to Fit" and therefore ignored
                steamEntryConstant = (steamEntryConstant*fitCount + fitVar[0])/(fitCount+1)
                steamInput = (steamInput*fitCount + steamInputI)/(fitCount+1)
                fitCount = fitCount + 1
                #print("Steam Input = %0.3f" % steamInput)

    return [steamEntryConstant,steamInput]

def fitRise(time,trace,indexQueue,decayConst,_steamOnTime,_steamCycleTime,plot=0): 
    #Main function for fitting the increases of RH due to steam with a decay chain like physics model
    #Takes in as inputs a time and an RH trace as well as several flags:
        #indexQueue: Used to pass the local maxima found in fitDecays()
        #decayConst: Result of fitDecays(). Passed in for fitting
        #_steamOnTime, _steamCycleTime: Parameters from the steam cycle used to aid fitting
        #plot: Flag to include plotting of the fits against the data. 0 = don't plot, 1 = plot.
    #global GLBdecayConst
    #GLBdecayConst = decayConst
    #global GLBsteamMag
    #GLBsteamMag = 20*_steamOnTime

    steamEntryConstant = 0
    steamInput = 0
    fitCount = 0

    for i in range(len(indexQueue)):
        if(indexQueue[i]>3000):
            [PSfit,PSiFit] = fitPulsedSteam(time[indexQueue[i]-3000:indexQueue[i]],trace[indexQueue[i]-3000:indexQueue[i]],decayConst,_steamCycleTime,2.5,plot)
            if(PSfit>0):        #Decays in RH from a maximum value of ignoreThreshold or less are regarded as small amounts of steam drips and therefore ignored
                steamEntryConstant = (steamEntryConstant*fitCount + PSfit)/(fitCount+1)
                steamInput = (steamInput*fitCount + PSiFit)/(fitCount+1)
                fitCount = fitCount + 1

            #print("Steam Entry Constant: %2.2f" % PSfit)
            #print("Steam Input: %2.2f" % PSiFit)
    return [steamEntryConstant,steamInput]

def ppH20vTime(time,_steamInput,_steamEntryConstant,_decayConstant,freqInterval,indexSteamOn,indexSteamOff,_DBtemp,compensate=1): 
        #Linear forward difference model for calculating water vapor fraction for an oven given the times when steam is injected and the steam characteristics of the oven.
        #Frequency Interval is How Often to Inject Steam in Minutes

    if(len(indexSteamOn) != len(indexSteamOff)):
        raise IndexError("Index Arrays Must Be Same Length")

    indexSteamOn = np.sort(indexSteamOn)
    indexSteamOff = np.sort(indexSteamOff)

    steamCycleCount =0
    flagSteamActive = 0

    dt = time[1] - time[0]
    N0 = np.zeros(len(time))
    N1 = np.zeros(len(time))
    Nsa = np.zeros(len(time))

    tInt = math.floor(freqInterval/dt)

    N0[0] = 0 #_steamInput

    dN0dt = 0
    dN1dt = 0

    for i in range(1,len(time)):
        dN0dt = -_steamEntryConstant*N0[i-1]
        dN1dt = _steamEntryConstant*N0[i-1] - _decayConstant*N1[i-1] + minivalaDBWBRate(_DBtemp[i])*_decayConstant*compensate

        if(i == indexSteamOn[steamCycleCount]):
            flagSteamActive = 1
        if(i == indexSteamOff[steamCycleCount]):
            flagSteamActive = 0
            steamCycleCount+=1
            if(steamCycleCount==len(indexSteamOn)):
                steamCycleCount-=1

        if(flagSteamActive == 1):
            Nsa[i] = 50 
        if(flagSteamActive == 1 and ((i-indexSteamOn[steamCycleCount])%tInt == 0)):
            N0[i] = N0[i-1] + dN0dt*dt + _steamInput
        else:
            N0[i] = N0[i-1] + dN0dt*dt
        N1[i] = N1[i-1] + dN1dt*dt
        if(N1[i]>100):
            N1[i] = 100

    return [time,N0,N1,Nsa]

def minivalaDBWBRate(_temp): #Return the expected water vapor fraction in oven (in %) from evaporation from DBWB humidity sensor
    _rate = (0.74*_temp-68.476)*((0.74*_temp-68.476)>0)
    return _rate

def classicppH20vTime(cycle,_decayConstant,_steamInRate,_steamDelay): 
        #Linear forward difference model for calculating water vapor fraction for an oven given a steam injection rate and the steam characteristics of the oven.
        #Frequency Interval is How Often to Inject Steam in Minutes

    totTime = np.zeros(1)
    for i in range(len(cycle)):
        totTime = np.append(totTime, totTime[len(totTime)-1] + cycle[i][0])    

    time = np.linspace(0,totTime[len(totTime)-1],totTime[len(totTime)-1]*100)
    dt = time[1] - time[0]

    N1 = np.zeros(len(time))
    Nsa = np.zeros(len(time))

    dN1dt = 0

    for i in range(1,len(totTime)):
        for j in range(len(time)):
            if i-2>0:
                if cycle[i-2][1]>0:
                    if time[j]>totTime[i-1] and time[j]<totTime[i]:
                        Nsa[j] = cycle[i-1][1]
                else:
                    if time[j]>(totTime[i-1]+_steamDelay) and time[j]<totTime[i]:
                        Nsa[j] = cycle[i-1][1]
            else:
                if time[j]>(totTime[i-1]+_steamDelay) and time[j]<totTime[i]:
                    Nsa[j] = cycle[i-1][1]

    for j in range(len(time)):
        if time[j]>0 and time[j]<totTime[0]:
            Nsa[j] = cycle[0][1]

    for i in range(1,len(time)):
        dN1dt = _steamInRate*Nsa[i-1] - _decayConstant*N1[i-1]

        N1[i] = N1[i-1] + dN1dt*dt
        if(N1[i]>100):
            N1[i] = 100

        if(N1[i]<0):
            N1[i] = 0

    return [time,N1,Nsa]

def minivalaBetappH20vTime(cycle,_boilrate,_decayConstant,_steamDelay,_boilerRampRate): 
        #Linear forward difference model for calculating water vapor fraction for an oven given a steam injection rate and the steam characteristics of the oven.

    totTime = np.zeros(1)
    for i in range(len(cycle)):
        totTime = np.append(totTime, totTime[len(totTime)-1] + cycle[i][0])    

    time = np.linspace(0,totTime[len(totTime)-1],totTime[len(totTime)-1]*100)
    dt = time[1] - time[0]

    N1 = np.zeros(len(time))+5/100
    Nsa = np.zeros(len(time))
    Tblr = np.zeros(len(time))

    dN1dt = 0

    for i in range(1,len(totTime)):
        for j in range(len(time)):
            if i-2>0:
                if cycle[i-2][1]>0:
                    if time[j]>totTime[i-1] and time[j]<totTime[i]:
                        Nsa[j] = cycle[i-1][1]
                else:
                    if time[j]>(totTime[i-1]+_steamDelay) and time[j]<totTime[i]:
                        Nsa[j] = cycle[i-1][1]
            else:
                if time[j]>(totTime[i-1]+_steamDelay) and time[j]<totTime[i]:
                    Nsa[j] = cycle[i-1][1]

    for j in range(len(time)):
        if time[j]>0 and time[j]<totTime[0]:
            Nsa[j] = cycle[0][1]

    for i in range(1,len(time)):
        dN1dt = _boilrate*math.exp(9*Tblr[i-1])/math.exp(9)*Nsa[i-1]*(1-N1[i-1]) - _decayConstant*N1[i-1] #-9*math.exp(-time[i]/_boilerRampRate)
        dTblrdt = Nsa[i-1]/_boilerRampRate-1/_boilerRampRate*Tblr[i-1]
        
        Tblr[i] = Tblr[i-1] + dTblrdt*dt

        N1[i] = N1[i-1] + dN1dt*dt

    return [time,N1,Nsa,Tblr]