# -*- coding: utf-8 -*-
"""
Created on Fri Feb 2 11:06:18 2018

The Packaging Loading Library contains the functions necessary to perform data analysis on
a series of boxes shipped around the country to analyze and refine loading curves (as of 
5/29/18 this means DD32 curves) for stereotypical shipments to the various ZIP codes that
Tovala ships to. 

@author: Peter
"""
from __future__ import division
import csv
import numpy as np
import scipy.signal as signalproc
import math

def smoothByAvg(x,avgLen):
    y=np.zeros(len(x)-avgLen)
    for i in range(len(x)-avgLen):
        y[i]=np.sum(x[i:i+avgLen])/avgLen
    #y=FtoC(y)
    return y

def smoothTrace(trace,savgolFilterWindow):

    trace = signalproc.savgol_filter(trace,savgolFilterWindow,2)
    return trace

def rollVar(currentCount,nextPoint,prevVar,prevAvg):
	currAvg = (prevAvg*(currentCount-1)+nextPoint)/currentCount
	newVariance = ((currentCount-1)*(prevVar+np.power(prevAvg,2))-2*(currentCount-1)*prevAvg*currAvg + np.power(nextPoint,2)-2*nextPoint*currAvg)/currentCount+np.power(currAvg,2)
	return [newVariance,currAvg]

def rollAvg(currentCount,nextPoint,prevAvg):
	currAvg = (prevAvg*(currentCount-1)+nextPoint)/currentCount
	return currAvg

def derivNearPoint(x,y,yTarget,ptSpread): #Method of search is to find positive derivative near first point that exceeds yTarget

    index = 0

    for i in range(1,len(y)):
        if((y[i]>yTarget) and index == 0):
            index = i
            break

    deriv = (y[index+ptSpread]-y[index-ptSpread])/(x[index+ptSpread]-x[index-ptSpread])

    return deriv

def detectLocalMaxima(trace,savgolFilterWindow = 0):  #Utility function to find all of the local maxima in a trace
	if(savgolFilterWindow==0):
		indexMax = signalproc.argrelextrema(trace,np.greater)
	else:
		indexMax = signalproc.argrelextrema(signalproc.savgol_filter(trace,savgolFilterWindow,2),np.greater)

	return indexMax

def detectLocalMinima(trace,savgolFilterWindow = 0):   #Utility function to find all of the local minima in a trace
	if(savgolFilterWindow==0):
		indexMin = signalproc.argrelextrema(trace,np.less)
	else:
		indexMin = signalproc.argrelextrema(signalproc.savgol_filter(trace,savgolFilterWindow,2),np.less)

	return indexMin

def prepArray(_array):
    _array=np.asarray(_array)
    _array=_array.astype(float)
    return _array
    
def prepData(x,lengthofAvg):
    x=np.asarray(x)
    x=x.astype(float)
    x=rollAvg(x,lengthofAvg);
    return x

def prepDataWithDither(x,lengthofAvg,ditherThreshold):
    x=np.asarray(x)
    x=x.astype(float)
    x = cleanDither(x,ditherThreshold)
    x=rollAvg(x,lengthofAvg);
    return x

def cleanDither(tTrace,ditherThreshold):
    lastValidIndex = 0
    dithering = 0
    cleanTTrace = np.zeros(len(tTrace))

    for i in range(3,len(tTrace)-3):
        if(tTrace[i]>1500):
            tTrace[i]=tTrace[i-1]
        if(math.fabs(tTrace[i-2]-tTrace[i-3])>ditherThreshold or math.fabs(tTrace[i-1]-tTrace[i-2])>ditherThreshold or math.fabs(tTrace[i]-tTrace[i-1])>ditherThreshold or math.fabs(tTrace[i+1]-tTrace[i])>ditherThreshold or math.fabs(tTrace[i+2]-tTrace[i+1])>ditherThreshold or math.fabs(tTrace[i+3]-tTrace[i+2])>ditherThreshold):
            dithering = 1
        else:
            dithering = 0
        if(dithering):
            if((tTrace[i+1]-tTrace[i])<(tTrace[i]-tTrace[i-1]) and (tTrace[i+1]-tTrace[i])>0 and math.fabs(tTrace[lastValidIndex]-tTrace[i])<20):
                if(lastValidIndex != i-1):
                    for j in range(lastValidIndex+1,i):
                        cleanTTrace[j]=(j-lastValidIndex)/(i-lastValidIndex)*(tTrace[i]-tTrace[lastValidIndex])+tTrace[lastValidIndex]
                lastValidIndex = i
                cleanTTrace[i] = tTrace[i]
        else:
            if(lastValidIndex != i-1):
                for j in range(lastValidIndex+1,i):
                    cleanTTrace[j]=(j-lastValidIndex)/(i-lastValidIndex)*(tTrace[i]-tTrace[lastValidIndex])+tTrace[lastValidIndex]
            cleanTTrace[i] = tTrace[i]
            lastValidIndex = i

    return cleanTTrace

def indexEdgeDetectBidirectional(time,signal,triggerVal,blankingTime):
    blankingFlag = 0
    indices = np.array([])
    direction = np.array([])
    if(len(time) != len(signal)):
        raise IndexError("Time and Signal Arrays Must Be Same Length")
    for i in range(1,len(signal)):
        if((0>((signal[i]-triggerVal)*(signal[i-1]-triggerVal))) and blankingFlag == 0):
            indices=np.append(indices,i)
            if(0>(signal[i]-triggerVal)):
                direction = np.append(direction,0)
            else:
                direction = np.append(direction,1)
            blankingFlag = 1
            #print(time[i])            
        if((blankingFlag == 1) and (time[i]-time[indices[len(indices)-1]])>blankingTime):
            blankingFlag = 0
            #print(time[i]-time[indices[len(indices)-1]])
    #print(indices)
    #print(direction)
    return [indices,direction]

def indexEdgeDetectRise(time,signal,triggerVal,blankingTime):
    blankingFlag = 0
    indices = np.array([])
    direction = np.array([])
    if(len(time) != len(signal)):
        raise IndexError("Time and Signal Arrays Must Be Same Length")
    for i in range(1,len(signal)):
        if((0>((signal[i]-triggerVal)*(signal[i-1]-triggerVal))) and blankingFlag == 0):
            if(0<(signal[i]-triggerVal)):
                direction = np.append(direction,1)
                indices=np.append(indices,i)
                blankingFlag = 1
            #print(time[i])            
        if((blankingFlag == 1) and (time[i]-time[indices[len(indices)-1]])>blankingTime):
            blankingFlag = 0
            #print(time[i]-time[indices[len(indices)-1]])
    #print(indices)
    #print(direction)
    return [indices,direction]
    
def indexEdgeDetectFall(time,signal,triggerVal,blankingTime):
    blankingFlag = 0
    indices = np.array([])
    direction = np.array([])
    if(len(time) != len(signal)):
        raise IndexError("Time and Signal Arrays Must Be Same Length")
    for i in range(1,len(signal)):
        if((0>((signal[i]-triggerVal)*(signal[i-1]-triggerVal))) and blankingFlag == 0):
            if(0>(signal[i]-triggerVal)):
                direction = np.append(direction,0)
                indices=np.append(indices,i)
                blankingFlag = 1
            #print(time[i])            
        if((blankingFlag == 1) and (time[i]-time[indices[len(indices)-1]])>blankingTime):
            blankingFlag = 0
            #print(time[i]-time[indices[len(indices)-1]])
    #print(indices)
    #print(direction)
    return [indices,direction]

def find2ndDerivLocalMaxima(_signal):  #Utility function for finding the local maxima of the second derivative of a trace. Used to find inflection points during steam injection portions

    _signal2ndDeriv = 300*3000*signalproc.savgol_filter(np.diff(signalproc.savgol_filter(np.diff(_signal),301,2)),301,2)
    indexMax = signalproc.argrelextrema(_signal2ndDeriv,np.greater)
    return indexMax[0]

def findPeriod(_time,_signal,verbose = False):
    avgSig = np.average(_signal)
    ampSig = (np.max(_signal) - np.min(_signal))/2

    hysteresis = ampSig/5
    hystFlag = True

    avgCrossings = []
    for i in range(1,len(_signal)):
        if((_signal[i]-avgSig)*(_signal[i-1]-avgSig)<=0 and hystFlag == False):
            if(verbose):
                print("Average Crossing Found at %02f" % _time[i])
            hystFlag = True
            avgCrossings = np.append(avgCrossings,i)
        elif(hystFlag == True):
            if((_signal[i]-avgSig+hysteresis)*(_signal[i-1]-avgSig+hysteresis)<=0 or (_signal[i]-avgSig-hysteresis)*(_signal[i-1]-avgSig-hysteresis)<=0):
                if(verbose):
                    print("Resetting Average Crossing at %02f" % _time[i])
                hystFlag = False

    _avgCrossingTimes = _time[avgCrossings.astype(int)]

    if(len(_avgCrossingTimes)%2==1):
        period = 2*np.average(np.diff(_avgCrossingTimes))
    else:
        period = 2*np.average(np.diff(_avgCrossingTimes[1:-1]))

    return period