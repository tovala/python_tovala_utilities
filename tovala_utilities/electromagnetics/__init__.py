# -*- coding: utf-8 -*-
"""
Electromagnetics solving utilities. As of the writing of this README (3/14/19),
the only electromagnetic solver in here is to determine the capacitance between
two coplanar plates given a spacing and width of the plates.

@author: Peter
"""

from __future__ import division
import csv
import numpy as np
import matplotlib.pylab as plt
import math


def coplanarPlateCapacitance(_spacing, _plateWidth,plotIt = 0):
    chargeLambda = 1
    eps0 = 1
    spacing = _spacing
    plateWidth = _plateWidth
    dimNum = 200

    xr = -spacing/2
    xl = -spacing/2 - plateWidth
    xl1 = spacing/2
    xr1 = spacing/2 + plateWidth

    A1 = chargeLambda/(xr-xl)/(2*math.pi*eps0)
    A2 = -chargeLambda/(xr1-xl1)/(2*math.pi*eps0)

    c1 = 2*chargeLambda/(2*math.pi*eps0)

    x = np.linspace(-spacing/2-plateWidth/2,spacing/2+plateWidth/2,dimNum)
    y = np.linspace(0.001,5*spacing,dimNum)
    #print(len(x))
    #print(len(y))
    Ex = np.zeros((len(x),len(y)))
    Ey = np.zeros((len(x),len(y)))

    for i in range(0,len(x)):
        for j in range(0,len(y)):
            Ex[i,j] = -A1/2*np.log(np.absolute(np.power(y[j],2)+np.power((x[i]-xr),2))/np.absolute(np.power(y[j],2)+np.power((x[i]-xl),2)))-A2/2*np.log(np.absolute(np.power(y[j],2)+np.power((x[i]-xr1),2))/np.absolute(np.power(y[j],2)+np.power((x[i]-xl1),2)))
            Ey[i,j] = -A1*(np.arctan2((x[i]-xr),y[j])-np.arctan2((x[i]-xl),y[j]))-A2*(np.arctan2((x[i]-xr1),y[j])-np.arctan2((x[i]-xl1),y[j]))
        #print("Row = %0d" % i)

    magE = np.sqrt(np.power(Ex,2)+np.power(Ey,2))

    #Trapezoidal Voltage Calculation
    volts = np.trapz(Ey[0,0:math.floor(dimNum/2)],y[0:math.floor(dimNum/2)])+np.trapz(Ex[:,math.floor(dimNum/2)],x)-np.trapz(Ey[dimNum-1,0:math.floor(dimNum/2)],y[0:math.floor(dimNum/2)])
    #volts1 = np.trapz(Ey[0,0:math.floor(dimNum/2)],y[0:math.floor(dimNum/2)])
    #volts2 = np.trapz(Ex[:,math.floor(dimNum/2)],x)
    #volts3 = -np.trapz(Ey[dimNum-1,0:math.floor(dimNum/2)],y[0:math.floor(dimNum/2)])
    caps = chargeLambda/volts
    print("Volts = %0.4f" % volts)
    print("Capacitance = %0.4f" % caps)
    #print("Volts2 = %0.4f" % volts2)
    #print("Volts3 = %0.4f" % volts3)

    if(plotIt != 0):
        fig1=plt.figure()
        plt.title("Ex")
        #plt.axis("OFF")
        ax1=fig1.add_subplot(1,1,1)
        CS=ax1.contourf(x,y,np.transpose(Ex))
        plt.clabel(CS)
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.colorbar(CS)

        fig2=plt.figure()
        plt.title("Ey")
        #plt.axis("OFF")
        ax2=fig2.add_subplot(1,1,1)
        CS=ax2.contourf(x,y,np.transpose(Ey))
        #plt.clabel(CS)
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.colorbar(CS)


        fig3=plt.figure()
        plt.title("E Field")
        #plt.axis("OFF")
        ax3=fig3.add_subplot(1,1,1)
        CS=ax3.quiver(x,y,np.transpose(Ex),np.transpose(Ey),np.transpose(magE))
        #plt.clabel(CS)
        plt.xlabel('X')
        plt.ylabel('Y')

    return caps