from __future__ import division
import numpy as np
import math
import datetime
from .. import data_import as datIn

def movingAverage(_averageCount,_oldAvg,_nextPoint):
    _newAvg = (_averageCount*_oldAvg+_nextPoint)/(_averageCount+1)
    return _newAvg

def mapTimeArray(tFrom, data, tTo,percentFlag = 1):
    #tTo must be monotonically increasing for this function to operate as intended
    #len(tFrom) must be equal to len(data), but the order of the points can be random in time

    dataMap = np.zeros(len(tTo))
    binCount = np.zeros(len(tTo))

    for j in range(len(tFrom)):
        if tFrom[j]<=tTo[0]:
            dataMap[0] = movingAverage(binCount[0],dataMap[0],data[j])
            binCount[0] = binCount[0] + 1
        else:
            index = np.amax(np.nonzero((tFrom[j]-tTo)>=0))+1
            if(index!=len(tTo)):
                dataMap[index] = movingAverage(binCount[index],dataMap[index],data[j])
                binCount[index] = binCount[index] + 1

    if (binCount[0] == 0):
        dataMap[0] = dataMap[1]

    for i in range(1,len(tTo)):
        if (binCount[i] == 0):
            nextNonzeroIndexArray = np.nonzero(dataMap)[0]*(np.nonzero(dataMap)[0]>i) #next nonzero index
            try:
                nextNonzeroIndex = nextNonzeroIndexArray[np.nonzero(nextNonzeroIndexArray)][0]
            except IndexError:
                nextNonzeroIndex = i-1
            dataMap[i] = (dataMap[i-1]+dataMap[nextNonzeroIndex])/2

    if (binCount[-1] == 0):
        dataMap[-1] = dataMap[-2]
        
    return [dataMap,binCount]

#mapTimeArrayQuick is a function that iterates through a time array (tTo), finds the nearest point in time on an array of data (data), and copies that point to
#a new array (dataMap) that corresponds to the new time array (tTo).
def mapTimeArrayQuick(tFrom, data, tTo):
    dataMap = np.zeros(len(tTo))
    for i in range(len(tTo)):
        index = np.abs(tTo[i]-tFrom).argmin()
        dataMap[i] = data[index]

    return dataMap

#Down sample a signal by averaging. Averaging will operate on groups of data in increments of reductionFactor.
#Extra data points points beyond integer numbers of reductionFactor bins will be dropped entirely.
def downSampleSignal(signal,reductionFactor):
    endIndex = -1*(len(signal) % reductionFactor)
    if(endIndex == 0):
        endIndex = len(signal)
    signal = signal[0:endIndex].reshape(-1, reductionFactor).mean(axis=1)

    return signal

#Returns the indices of the points closest to a series of thresholds.
#Will return the closest point, so if cyclical data is used, only the closest point to the threshold will be returned.
def timeIndices(time, cutoffs):
	cutoffIndices = []

	index = 0

	for i in cutoffs:
		index = np.argmin(np.abs(time-i))
		cutoffIndices.append(index)

	return cutoffIndices

def timestampFromDateAndTime(x,y):
    month = int(x.split("/")[0])
    day = int(x.split("/")[1])
    year = int(x.split("/")[2])
    hour = int(y.split(":")[0])
    minute = y.split(":")[1]
    try:
        second = y.split(":")[2]
        try:
            if(hour != 12 and second.split(" ")[1] == 'PM'):
                hour = hour + 12
            elif(hour == 12 and second.split(" ")[1] == 'AM'):
                hour = hour - 12
            second = int(second.split(" ")[0])
        except IndexError:
            second = int(second)
        minute = int(minute)
    except IndexError:
        try:
            if(hour != 12 and minute.split(" ")[1] == 'PM'):
                hour = hour + 12
            elif(hour == 12 and minute.split(" ")[1] == 'AM'):
                hour = hour - 12
            minute = int(minute.split(" ")[0])
        except IndexError:
            minute = int(minute)
        second = 0


    #print("Year = %d, Month = %d, Day = %d, Hour = %d, Minute = %d, Second = %d" % (year,month,day,hour,minute,second))
    #if(minute.split(" ")[1] == 'PM'):
        #hour = hour + 12
    #elif(hour == 12):
        #hour = hour - 12
    #minute = int(minute.split(" ")[0])
    #second = 0

    timestamp = ((datetime.datetime(year,month,day,hour,minute,second)-datetime.datetime(1970,1,1)).total_seconds())

    return timestamp
