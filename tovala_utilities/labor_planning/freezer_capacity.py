from __future__ import division
import numpy as np
import math
import datetime
from optparse import OptionParser
#from ..constants import *
#from . import constants

MEAL_CT_75K = 75000
N_3MEAL_BOXES_75K = 2200*75/81
N_4MEAL_BOXES_75K = 6000*75/81
N_6MEAL_BOXES_75K = 6000*75/81
N_8MEAL_BOXES_75K = 1800*75/81
FRAC_SLC = 0.28
FRAC_LEW = 0.22
FRAC_OLB = 0.14
FRAC_UDS = 0.12
FRAC_CHI = 0.24
FRAC_1ST_PACKOUT_DAY = FRAC_SLC+FRAC_LEW+FRAC_OLB+FRAC_UDS
FRAC_2ND_PACKOUT_DAY = FRAC_CHI
DAYS = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
ZEROS_BY_DAY={'Sunday': 0, 'Monday': 0, 'Tuesday': 0, 'Wednesday': 0, 'Thursday': 0, 'Friday': 0, 'Saturday': 0}
DEFAULT_COLOR = 'Blue'
DEFAULT_COLOR_BY_DAY = {'Sunday': 'Blue', 'Monday': 'Blue', 'Tuesday': 'Blue', 'Wednesday': 'Blue', 'Thursday': 'Blue', 'Friday': 'Blue', 'Saturday': 'Blue'}


#ASSUMPTIONS ABOUT FREEZER CAPACITY:

N_PALLETS_RACKING = 54 #Number of pallets that can fit into pallet racking spaces on the floor at Tubeway
N_PALLETS_FLOOR = 80 #Number of pallets that can fit on the floor at Tubeway

N_3MEAL_ICE_PER_PALLET = 50*10
N_4MEAL_ICE_PER_PALLET = 56*7

N_SALMON_PER_PALLET = 2880
N_PALLETS_SALMON_75K = 1.5*0.14*75000/N_SALMON_PER_PALLET #Require 1.5 weeks of inventory on site, estimate 14% of menu
N_CHICKEN_PER_PALLET = 3960
N_PALLETS_CHICKEN_75K = 1.5*0.31*75000/N_CHICKEN_PER_PALLET #Require 1.5 weeks of inventory on site, estimate 31% of menu
N_PORK_PER_PALLET = 3960
N_PALLETS_PORK_75K = 1.5*0.1*75000/N_PORK_PER_PALLET #Require 1.5 weeks of inventory on site, estimate 10% of menu

N_PALLETS_BISCUITS_MEATLOAF_75k = 9 #Estiamte based on current tower space requirements for biscuits and meatloaf

MAX_N_PALLETS_ICE_75K = ((2*N_3MEAL_BOXES_75K+4*N_6MEAL_BOXES_75K)/N_3MEAL_ICE_PER_PALLET+(2*N_4MEAL_BOXES_75K+4*N_8MEAL_BOXES_75K)/N_4MEAL_ICE_PER_PALLET)*1.10 #Number of pallets of ice req'd at 75k meals

MAX_N_PALLETS_BULK_FOOD_75K = 43 #Estimate based on pre-vac pack usage of freezer.

N_PALLETS_VAC_PACK_75k = N_PALLETS_SALMON_75K + N_PALLETS_CHICKEN_75K + N_PALLETS_PORK_75K

FREEZER_OPTIONS = [
	'ON_DEMAND_ICE', #Flag to enable/disable on-demand ice
	'VAC_PACK_ONLY', #Only store ice and vac pack proteins onsite
	'FOOD_STORAGE_SPLIT_25-75', #Store ice, vac pack proteins, and 25% of food onsite
	'FOOD_STORAGE_SPLIT_50-50',
	'FOOD_STORAGE_SPLIT_75-25',
	'FOOD_STORAGE_SPLIT_100-0' #Store ice, vac pack proteins, and all frozen food onsiteonsite
	'FULL_PALLET_REQUIREMENT'
]

#print('MAX ICE = %0.2f' % MAX_N_PALLETS_ICE_75K)

def showAvailableFreezerOptions():
	print(FREEZER_OPTIONS)

def calcCapacity(options, mealCount,batchSplit = {'Sunday': 1.0}):

	#Normalize Batch Split
	batchSplitTot = sum(batchSplit.values(), 0.0)
	batchSplit = {k: v/batchSplitTot for k,v in batchSplit.items()}

	#Calculate Pallet Load
	pallet_req = ZEROS_BY_DAY
	if('FULL_PALLET_REQUIREMENT' in options):
		print('Full Pallet Requirement = %0.2f' % ((MAX_N_PALLETS_ICE_75K+MAX_N_PALLETS_BULK_FOOD_75K+N_PALLETS_BISCUITS_MEATLOAF_75k+N_PALLETS_VAC_PACK_75k)*mealCount/MEAL_CT_75K))
	for shipDay in batchSplit:
		print("%s, %0.2f" % (shipDay,batchSplit[shipDay]))

		#ICE LOAD REQUIREMENTS
		#Append ice loads
		if('ON_DEMAND_ICE' in options):
			for day in enumerate(pallet_req):
				if(day[1] ==shipDay):
					pallet_req[DAYS[day[0]]] += MAX_N_PALLETS_ICE_75K*FRAC_1ST_PACKOUT_DAY*batchSplit[shipDay]
					pallet_req[DAYS[day[0]+1]] += MAX_N_PALLETS_ICE_75K*FRAC_2ND_PACKOUT_DAY*batchSplit[shipDay]
		else:
			if(shipDay=='Saturday'):
				pallet_req['Friday'] += MAX_N_PALLETS_ICE_75K*batchSplit[shipDay]
			if(shipDay=='Sunday'):
				pallet_req['Friday'] += MAX_N_PALLETS_ICE_75K*batchSplit[shipDay]
				pallet_req['Saturday'] += MAX_N_PALLETS_ICE_75K*batchSplit[shipDay]
			for day in enumerate(pallet_req):
				if(day[1] ==shipDay):
					pallet_req[DAYS[day[0]]] += MAX_N_PALLETS_ICE_75K*batchSplit[shipDay]
					pallet_req[DAYS[day[0]+1]] += MAX_N_PALLETS_ICE_75K*FRAC_2ND_PACKOUT_DAY*batchSplit[shipDay]

		#FROZEN COMPONENT AND FROZEN FOOD LOAD REQUIREMENTS
		#Append frozen component and bulk food space required
		N_PALLETS_BULK_FOOD_75K = 0.66*MAX_N_PALLETS_BULK_FOOD_75K
		if('VAC_PACK_ONLY' in options):
			N_PALLETS_BULK_FOOD_75K = 0
		if('FOOD_STORAGE_SPLIT_25-75' in options):
			N_PALLETS_BULK_FOOD_75K = 0.25*MAX_N_PALLETS_BULK_FOOD_75K
		if('FOOD_STORAGE_SPLIT_50-50' in options):
			N_PALLETS_BULK_FOOD_75K = 0.5*MAX_N_PALLETS_BULK_FOOD_75K
		if('FOOD_STORAGE_SPLIT_75-25' in options):
			N_PALLETS_BULK_FOOD_75K = 0.75*MAX_N_PALLETS_BULK_FOOD_75K
		if('FOOD_STORAGE_SPLIT_100-0' in options):
			N_PALLETS_BULK_FOOD_75K = MAX_N_PALLETS_BULK_FOOD_75K

		#Add meatloaf and biscuit towers
		N_PALLETS_BULK_FOOD_75K += N_PALLETS_BISCUITS_MEATLOAF_75k

		if(shipDay=='Saturday'):
			pallet_req['Friday'] += N_PALLETS_VAC_PACK_75k*batchSplit[shipDay]
		if(shipDay=='Sunday'):
			pallet_req['Friday'] += N_PALLETS_VAC_PACK_75k*batchSplit[shipDay]
			pallet_req['Saturday'] += N_PALLETS_VAC_PACK_75k*batchSplit[shipDay]
		for day in enumerate(pallet_req):
			pallet_req[day[1]] += N_PALLETS_BULK_FOOD_75K*batchSplit[shipDay]
			if(day[1] ==shipDay):
				pallet_req[DAYS[day[0]]] += N_PALLETS_VAC_PACK_75k*batchSplit[shipDay]
				pallet_req[DAYS[day[0]+1]] += N_PALLETS_VAC_PACK_75k*FRAC_2ND_PACKOUT_DAY*batchSplit[shipDay]

	pallet_req = {k: v*mealCount/MEAL_CT_75K for k,v in pallet_req.items()}

	return pallet_req


def calcLabor(): #In person hours per day
	return ZEROS_BY_DAY

#%%
if __name__ == "__main__":
	parser = OptionParser()
	parser.add_option("-o", "--on-demand-ice", dest="odi", action="store_true",
					  help="On Demand Ice", default=False)
	parser.add_option("-f", "--full-pallet-req", dest="fpr", action="store_false",
					  help="Full Pallet Requirement", default=True)
	parser.add_option("-d", "--dual-ship", dest="ds", action="store_true",
					  help="Enable Dual Ship Days", default=False)

	(parserOptions, args) = parser.parse_args()

	options = ['FOOD_STORAGE_SPLIT_75-25']
	if(parserOptions.odi):
		options.append('ON_DEMAND_ICE')
	if(parserOptions.fpr):
		options.append('FULL_PALLET_REQUIREMENT')
	if(parserOptions.ds):
		print(calcCapacity(options,75000,batchSplit = {'Sunday': 0.8, 'Tuesday': 0.2}))
	else:
		print(calcCapacity(options,72000))
