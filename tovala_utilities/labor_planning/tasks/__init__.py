from __future__ import division
import numpy as np
import math
import datetime
import copy
from ..constants import *
#from .. import data_import as datIn

class Task:

    def __init__(self,_name,_process,_output_unit,_totalCount,_location,_hours_per_batch = [],_meal = 0,_parent = "",_target_weight = 0.0):
        self.name = _name
        if(_process in validProcesses):
            self.process = _process
        else:
            raise ValueError("Process Not Recognized for Task: %s" % _name)
        if(_output_unit in validOutputUnits):
            self.output_unit = _output_unit
        else:
            raise ValueError("Output Unit Not a Valid Process Output for Task: %s" % _name)
        self.totalCount = _totalCount
        self.dailyCount = copy.deepcopy(ZEROS_BY_DAY)
        self.location = _location
        self.hours_per_batch = _hours_per_batch
        self.meal = _meal
        self.parent = _parent
        self.target_weight = _target_weight
        self.assigned_equipment = []