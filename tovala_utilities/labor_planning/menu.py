from __future__ import division
import numpy as np
import math
import datetime
from optparse import OptionParser
#from . import constants

class MenuObject:

    def __init__(self,_meals,_trays,_vac_pack,_garnishes,_breads_per_meal):
        self.meals = _meals
        self.trays = _trays
        self.vac_pack = _vac_pack
        self.garnishes = _garnishes
        self.breads_per_meal = _breads_per_meal


class MenuTraysObject:

    def __init__(self,_trays_per_meal,_salads_per_meal,_veg_bag_per_meal,_grains_per_meal,_biscuits_per_meal,_t1_cook_tray_per_meal,_t2_cook_tray_per_meal,_nt_cook_tray_per_meal):
        self.trays_per_meal = _trays_per_meal
        self.salads_per_meal = _salads_per_meal
        self.veg_bag_per_meal = _veg_bag_per_meal
        self.grains_per_meal = _grains_per_meal
        self.biscuits_per_meal = _biscuits_per_meal
        self.t1_cook_tray_per_meal = _t1_cook_tray_per_meal #1 drop tray (transportable), requiring cooking, per meal
        self.t2_cook_tray_per_meal = _t2_cook_tray_per_meal #2 drop tray (transportable), requiring cooking, per meal
        self.nt_cook_tray_per_meal = _nt_cook_tray_per_meal #non-transportable, requiring cooking, per meal

class MenuGarnishesObject:

    def __init__(self,_garnishes_per_meal,_cheeses_per_meal,_packets_per_meal,_sachets_per_meal,_cups_directportion_per_meal,_cups_processed_per_meal):
        self.trays_per_meal = _trays_per_meal
        self.cheeses_per_meal = _cheeses_per_meal
        self.packets_per_meal = _packets_per_meal
        self.sachets_per_meal = _sachets_per_meal
        self.cups_directportion_per_meal = _cups_directportion_per_meal
        self.cups_processed_per_meal = _cups_processed_per_meal


#Term 175-179 Standard Menu:
average_menu_terms175_179 = MenuObject(14.6,MenuTraysObject(1.4,0.14,0.29,0.15,0.06,0.33,0.21,0.23),0.54,MenuGarnishesObject(2.61,0.31,0.42,0.38,0.35,1.14),0.11)
mins_menu_terms175_179 = MenuObject(14,MenuTraysObject(1.36,0.07,0.13,0.07,0.00,0.21,0.13,0.13),0.47,MenuGarnishesObject(2.33,0.14,0.27,0.20,0.21,0.93),0.07)
maxs_menu_terms175_179 = MenuObject(15,MenuTraysObject(1.43,0.20,0.36,0.21,0.13,0.53,0.29,0.40),0.57,MenuGarnishesObject(3.14,0.43,0.71,0.53,0.67,1.29),0.14)