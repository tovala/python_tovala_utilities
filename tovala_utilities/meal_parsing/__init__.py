# -*- coding: utf-8 -*-
"""
Created on Wed May 24 11:18:51 2017

@author: Peter
"""

from __future__ import division
import csv
import numpy as np
import matplotlib.pylab as plt
import scipy.optimize as optimization
#import scipy.signal as signalproc
import pprint as pp
import math
import urllib3 as url
import json
import datetime
from . import ta_parsing as taP
from .. import data_import as datIn

TA_CDN_URL = 'http://cdn.tovala.com/assist/recipes.json'
#TA_CDN_URL = 'http://cdn.dev.tovala.com/assist/recipes_press.json' #For 10/29/18 (week of) Press Tour

class mealObject:

    def __init__(self,_name,_ID,_routine,_routineIndex):
        self.name = _name
        self.id = _ID
        self.routine = _routine
        self.routineIndex = _routineIndex

class gen1directStep:

    def __init__(self,_fan,_top,_steam,_bottom):
        self.fan = _fan
        self.top = _top
        self.steam = _steam
        self.bottom = _bottom

    def reprJSON(self):
        return dict(fan = self.fan, top = self.top, steam = self.steam, bottom = self.bottom)


class gen1RoutineStep:

    def __init__(self,_fan,_top,_steam,_bottom,_cookTime,_temperature):
        self.direct = gen1directStep(_fan,_top,_steam,_bottom)
        self.cookTime = _cookTime
        self.temperature = _temperature

    def reprJSON(self):
        return dict(direct = self.direct, cookTime = self.cookTime, temperature = self.temperature)

class gen2RoutineStep:

    def __init__(self,_steam,_top,_broil,_bottom,_fan,_cookTime,_temperature):
        self.steam = _steam
        self.top = _top
        self.broil = _broil
        self.bottom = _bottom
        self.fan = _fan
        self.cookTime = _cookTime
        self.temperature = _temperature

    def reprJSON(self):
        return dict(steam = self.steam, top = self.top, broil = self.broil, bottom = self.bottom, fan = self.fan, cookTime = self.cookTime, temperature = self.temperature)

class generalSchemaRoutineStep:

    def __init__(self,_mode,_duration,_temperature):
        self.mode = _mode
        self.duration = _duration
        self.temperature = _temperature

    def reprJSON(self):
        return dict(mode = self.mode, duration = self.duration, temperature = self.temperature)


def getCDNData(routines = False, routineTotalTimes = False):
    
    #Make the Request and Return An Array of Recipe Names
    http = url.PoolManager()
    r = http.request('GET', TA_CDN_URL, headers={'Content-Type': 'application/json'})
    k = json.loads(r.data)['recipes']

    #Log Recipes
    _names = []
    _routineTotalTimes = []
    _routines = []
    totalTime = 0

    for thing in k:
        _names.append(thing['title'])
        if(routineTotalTimes):
            totalTime = 0
            a = thing['directionSections']
            for dS in a:
                #print("Checking Directions for Routine")
                b = dS['directions']
                for direction in b:
                    if 'routine' in direction:
                        if(routines):
                            _routines.append(direction['routine'])
                        for step in direction['routine']['routine']:
                            totalTime = totalTime + step['cookTime']

        _routineTotalTimes.append(totalTime)
        
        #print("Start Time(s) = %s" % (datetime.datetime.fromtimestamp(thing['_source']['timestamp']).strftime('%Y-%m-%d %H:%M:%S')))
        
    return [_names,_routines,_routineTotalTimes]

def loadMealData(_filename = 'C:/Users/peter/Downloads/tovala meals as of 2019-01-10.csv', withIndices = False): #'C:/Users/peter/Downloads/tovala meals as of 2019-01-10.csv'
    [_index,_data,_] = datIn.importArbitraryCSV_1(_filename,headers = 1) 
    #print(len(_index))
    meals = []
    names = _data[0]
    routines = _data[2]
    if(withIndices):
        indices = np.asarray(_data[6])
    else:
        indices = []
    for i in range(1,len(_index)+1):
        if(withIndices):
            meals.append(mealObject(names[i],_index[i-1],json.loads(routines[i]),indices[i].astype(float)))
        else:
            meals.append(mealObject(names[i],_index[i-1],json.loads(routines[i]),-1))

    return meals

def compareMealCycles(meal1,meal2,verbose = False,raiseErrors = False):
    if(len(meal1.routine) > len(meal2.routine)):
        cycleLength = len(meal1.routine)
    else:
        cycleLength = len(meal2.routine)

    #print(cycleLength)
    cycleExact = True
    for i in range(cycleLength):
        try:
            step1 = meal1.routine['routine'][i]
            step2 = meal2.routine['routine'][i]
            if((step1['direct'] != step2['direct']) or (step1['temperature'] != step2['temperature']) or (step1['cookTime'] != step2['cookTime'])):
                cycleExact = False
            if(verbose):
                if(step1['direct'] != step2['direct']):
                    print("For Step %d, Type of Cycle is Different" %i)
                    print(step1['direct'])
                    print(step2['direct'])
                if(step1['temperature'] != step2['temperature']):
                    print("For Step %d, Temperature of Cycle is Different" %i)
                    print("First Recipe Temperature is: %0.1f" % step1['temperature'])
                    print("Second Recipe Temperature is: %0.1f" % step2['temperature'])
                if(step1['cookTime'] != step2['cookTime']):
                    print("For Step %d, CookTime of Cycle is Different" %i)
                    print("First Recipe Cook Time is: %0.1f" % step1['cookTime'])
                    print("Second Recipe Cook Time is: %0.1f" % step2['cookTime'])

            #print(step1['direct'] == step2['direct'])
            #print(step1['temperature'] == step2['temperature'])
            #rint(step1['cookTime'] == step2['cookTime'])

        except IndexError:
            cycleExact = False
            print("One Cycle is Longer than Another")
        except KeyError:
            cycleExact = False
            if(raiseErrors):
                raise ValueError('Improper Formatting of Input Routine')
            print("Comparing Routine with Improper Formetting")
            break

    return cycleExact
    #print(meals[0].name)
    #print(meals[0].id)
    #print(meals[0].routine['routine'][0]['direct'])

def FineMealCompare(meal1,meal2,verbose = False,raiseErrors = False):

    DS53_1 = 0
    cookTime1 = 0
    broilCount1 = 0
    lastStepBroilLength1 = 0
    firstStepSteam1 = False
    DS53_2 = 0
    cookTime2 = 0
    broilCount2 = 0
    lastStepBroilLength2 = 0
    firstStepSteam2 = False


    try:
        
        for i in range(len(meal1.routine['routine'])):
            step = meal1.routine['routine'][i]
            if(cookTime1>180):
                cookTime1 = cookTime1 + step['cookTime']
                if((step['direct']['top']<=50)):
                    DS53_1 = (step['temperature']-53)*step['cookTime'] + DS53_1
                    #print(DS53_1)
            else:
                if((step['direct']['top']<=50) and ((cookTime1 + step['cookTime'])>180)):
                    DS53_1 = (step['temperature']-53)*(step['cookTime']-180+cookTime1) + DS53_1
                    #print(step['temperature'])
                    #print(step['cookTime'])
                    #print(cookTime1)
                    #print(DS53_1)
                    cookTime1 = cookTime1 + step['cookTime']
                else:
                    cookTime1 = cookTime1 + step['cookTime']
            if(step['direct']['top']>50):
                broilCount1 = broilCount1 + 1
            if ((i==0) and (step['direct']['steam']==1)):
                firstStepSteam1 = True
            if ((i==(len(meal1.routine['routine'])-1)) and (step['direct']['top']>50)):
                lastStepBroilLength1 = step['cookTime']

        #print(DS53_1)


        for i in range(len(meal2.routine['routine'])):
            step = meal2.routine['routine'][i]
            if(cookTime2>180):
                cookTime2 = cookTime2 + step['cookTime']
                if((step['direct']['top']<=50)):
                    DS53_2 = (step['temperature']-53)*step['cookTime'] + DS53_2
                    #print(step['temperature'])
                    #print(step['cookTime'])
                    #print(cookTime2)
                    #print(DS53_2)
            else:
                if((step['direct']['top']<=50) and ((cookTime2 + step['cookTime'])>180)):
                    DS53_2 = (step['temperature']-53)*(step['cookTime']-180+cookTime2) + DS53_2
                    cookTime2 = cookTime2 + step['cookTime']
                else:
                    cookTime2 = cookTime2 + step['cookTime']
            if(step['direct']['top']>50):
                broilCount2 = broilCount2 + 1
            if ((i==0) and (step['direct']['steam']==1)):
                firstStepSteam2 = True
            if ((i==(len(meal2.routine['routine'])-1)) and (step['direct']['top']>50)):
                lastStepBroilLength2 = step['cookTime']

            #print(DS53_2)

    except KeyError:
        if(raiseErrors):
            raise ValueError('Improper Formatting of Input Routine')
        print("Comparing Routine with Improper Formetting")

    #Is the Total Cook Time Equivalent?
    #print(DS53_1)
    #print(DS53_2)
    cTEquiv = cookTime1 == cookTime2
    bCEquiv = broilCount1 == broilCount2
    bLSEquiv = lastStepBroilLength1 == lastStepBroilLength2
    DS53Ratio = np.abs(1- DS53_1/DS53_2)
    fSEquiv = firstStepSteam1 == firstStepSteam2    

    return [cTEquiv,bCEquiv,bLSEquiv,DS53Ratio,fSEquiv,DS53_1,DS53_2]

def MediumMealCompare(meal1,meal2,verbose = False,raiseErrors = False):

    DS53_1 = 0
    cookTime1 = 0
    broilCount1 = 0
    lastStepBroilLength1 = 0
    isSteam1 = False
    DS53_2 = 0
    cookTime2 = 0
    broilCount2 = 0
    lastStepBroilLength2 = 0
    isSteam2 = False


    try:
        
        for i in range(len(meal1.routine['routine'])):
            step = meal1.routine['routine'][i]
            if(cookTime1>180):
                cookTime1 = cookTime1 + step['cookTime']
                if((step['direct']['top']<=50)):
                    DS53_1 = (step['temperature']-53)*step['cookTime'] + DS53_1
                    #print(DS53_1)
            else:
                if((cookTime1 + step['cookTime'])>180):
                    if(step['direct']['top']>50):
                        temp = 500
                    else:
                        temp = step['temperature']
                    DS53_1 = (temp-53)*(step['cookTime']-180+cookTime1) + DS53_1
                    #print(step['temperature'])
                    #print(step['cookTime'])
                    #print(cookTime1)
                    #print(DS53_1)
                    cookTime1 = cookTime1 + step['cookTime']
                else:
                    cookTime1 = cookTime1 + step['cookTime']
            if(step['direct']['top']>50):
                broilCount1 = broilCount1 + 1
            if ((step['direct']['steam']==1)):
                isSteam1 = True
            if ((i==(len(meal1.routine['routine'])-1)) and (step['direct']['top']>50)):
                lastStepBroilLength1 = step['cookTime']

        #print(DS53_1)


        for i in range(len(meal2.routine['routine'])):
            step = meal2.routine['routine'][i]
            if(cookTime2>180):
                cookTime2 = cookTime2 + step['cookTime']
                if((step['direct']['top']<=50)):
                    DS53_2 = (step['temperature']-53)*step['cookTime'] + DS53_2
                    #print(step['temperature'])
                    #print(step['cookTime'])
                    #print(cookTime2)
                    #print(DS53_2)
            else:
                if((cookTime2 + step['cookTime'])>180):
                    if(step['direct']['top']>50):
                        temp = 500
                    else:
                        temp = step['temperature']
                    DS53_2 = (temp-53)*(step['cookTime']-180+cookTime2) + DS53_2
                    cookTime2 = cookTime2 + step['cookTime']
                else:
                    cookTime2 = cookTime2 + step['cookTime']
            if(step['direct']['top']>50):
                broilCount2 = broilCount2 + 1
            if ((step['direct']['steam']==1)):
                isSteam2 = True
            if ((i==(len(meal2.routine['routine'])-1)) and (step['direct']['top']>50)):
                lastStepBroilLength2 = step['cookTime']

            #print(DS53_2)

    except KeyError:
        if(raiseErrors):
            raise ValueError('Improper Formatting of Input Routine')
        print("Comparing Routine with Improper Formetting")

    #Is the Total Cook Time Equivalent?
    #print(DS53_1)
    #print(DS53_2)
    cTEquiv = cookTime1 == cookTime2
    bCEquiv = broilCount1 == broilCount2
    bLSEquiv = np.abs(lastStepBroilLength1 - lastStepBroilLength2) < 15
    DS53Ratio = np.abs(1- DS53_1/DS53_2)
    fSEquiv = isSteam1 == isSteam2
    #print(isSteam1)
    #rint(isSteam2)   

    return [cTEquiv,bCEquiv,bLSEquiv,DS53Ratio,fSEquiv,DS53_1,DS53_2]

def storeMealObjects(mealObjects,_csvfile = 'C:/Users/peter/Downloads/tovala standard meals.csv'):

    for meal in mealObjects:
        x = '"'+str(json.dumps(meal.routine))+'"'
        print(x)
        rowToWrite = [meal.id,meal.name,'',[x]]
        with open(_csvfile,'ab') as csvfile:
            data = csv.writer(csvfile, delimiter=',', quotechar='|');
            for i in range(1):
                #print ','.join(row)
                #print row[1:17]
                data.writerow(rowToWrite)
    
