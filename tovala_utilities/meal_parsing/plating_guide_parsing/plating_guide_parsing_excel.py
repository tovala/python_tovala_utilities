from __future__ import print_function
import csv
import numpy as np
import json
import openpyxl
from os import listdir
from os.path import isfile, join
import difflib

VALID_FACILITIES = {}
VALID_FILM_TYPES = []
VALID_TRAY_TYPES = []
VALID_MAP_BLENDS = []
VALID_PROJECTIONS = []
VALID_TRANSPORT_CONTAINERS = []
VALID_INVENTORY_TYPES = []
VALID_CYCLES = []
VALID_GARNISH_TYPES = []

PLATING_GUIDE_COLUMNS_MAPPING = {
    'v1': {
        'MealCode': 'A',
        'MealCount': 'B',
        'MealName': 'C',
        'SleevingLocation': 'AG',
        'FilmType': 33,
        'Tray1_Contents': 'E',
        'Tray1_PreppedAt': 5,
        'Tray1_PrepDay': 6,
        'Tray1_TransportedIn': 7, 
        'Tray1_PortionedAt': 'I',
        'Tray1_PortionDay': 'J',
        'Tray1_MAP': 'M',
        'Tray2_Contents': 13,
        'Tray2_PreppedAt': 14,
        'Tray2_PrepDay': 15,
        'Tray2_TransportedIn': 16, 
        'Tray2_PortionedAt': 17,
        'Tray2_PortionDay': 18,
        'Tray2_MAP': 21,
        'Garnish': 23,
        'Garnish_Container': 29,
        'Garnish_PreppedAt': 24,
        'Garnish_PrepDay': 25,
        'Garnish_TransportedIn': 26,
        'Garnish_PortionedAt': 27,
        'Garnish_PortionDay': 28    
    }
}

class MealObject_Detailed:

    def __init__(self,_id,_count,_meal,_sleeving_location,_sleeving_days):
        self.id = _id
        print("Creating Meal Object for Meal #: %d" % self.id)

        if isinstance(_count, MealCount):
            self.count = _count
        else:
            raise ValueError("Meal Object Created without Valid Count")

        if isinstance(_meal, MealMaster):
            self.meal = _meal
        else:
            raise ValueError("Meal Object Created without Valid Meal")

        if(_sleeving_location not in VALID_FACILITIES):
            self.sleeving_location = "Unknown"
        else:
            self.sleeving_location = _sleeving_location

        self.sleeving_days = {}
        for cycle in VALID_CYCLES:
            self.sleeving_days.update({cycle: "Unknown"})
        for cycle in _sleeving_days:
            if(cycle not in VALID_CYCLES):
                print("Warning Cycle %s Not Recognized" % cycle)
            else:
                if(_sleeving_days[cycle] not in VALID_FACILITIES[self.sleeving_location]):
                    self.sleeving_days[cycle] =  "Unknown"
                else:
                    self.sleeving_days[cycle] =  _sleeving_days[cycle]

class MealCount:

    def __init__(self, _projections, _final):
        self.projections = {}

        for line in _projections:
            if line in VALID_PROJECTIONS:
                self.projections.update({line: {}})
                for cycle in VALID_CYCLES:
                    self.projections[line].update({cycle: 0})
                for cycle in _projections[line]:
                    if(cycle not in VALID_CYCLES):
                        raise ValueError("<Projections Count> Cycle for Meal Count Not Recognized")
                    else:
                        self.projections[line][cycle] = int(_projections[line][cycle])
        self.final = {}
        for cycle in VALID_CYCLES:
            self.final.update({cycle: 0})
        for cycle in _final:
            if(cycle not in VALID_CYCLES):
                raise ValueError("WARNING: <Final Count> Cycle for Meal Count Not Recognized")
            else:
                self.final[cycle] = int(_final[cycle])

class MealMaster:

    def __init__(self, _name, _trays = [], _garnishes = []):
        
        self.name = _name
        self.trays = []
        for tray in _trays:
            if isinstance(tray, TrayObject):
                self.trays.append(tray)
            else:
                print("WARNING: Tray for Meal %s Not Recognized" % _name)

        self.garnishes = []
        for garnish in _garnishes:
            if isinstance(garnish, GarnishObject):
                self.garnishes.append(garnish)
            else:
                print("WARNING: Garnish for Meal %s Not Recognized" % _name)
    
    def addTray(self,tray):
        if isinstance(tray, TrayObject):
            self.trays.append(tray)
        else:
            print("WARNING: Tray for Meal %s Not Recognized" % self.name)

    def addGarnish(self, garnish):
        if isinstance(garnish, GarnishObject):
            self.garnishes.append(garnish)
        else:
            print("WARNING: Garnish for Meal %s Not Recognized" % self.name)



class TrayObject:

    def __init__(self,_portion_location,_portion_days,_film, _type, _map,_spareTin = False, _contents = []):
        self.frozen = False
        self.contents = []
        for item in enumerate(_contents):
            self.contents.append({"item": [], "portion_weight": 0.0, "expected_portion_time": -1.0})
            for field in item[1]:
                if field in self.contents[item[0]]:
                    self.contents[item[0]][field] = item[1][field]

        if(_portion_location not in VALID_FACILITIES):
            self.portion_location = "Unknown"
        else:
            self.portion_location = _portion_location

        self.portion_days = {}
        for cycle in VALID_CYCLES:
            self.portion_days.update({cycle: "Unknown"})
        for cycle in _portion_days:
            if(cycle not in VALID_CYCLES):
                print("Warning Cycle %s Not Recognized" % cycle)
            else:
                if(_portion_days[cycle] not in VALID_FACILITIES[self.portion_location]):
                    self.portion_days[cycle] =  "Unknown"
                else:
                    self.portion_days[cycle] =  _portion_days[cycle]

        if(_film not in VALID_FILM_TYPES):
            self.film = "Unknown"
        else:
            self.film = _film

        if(_type not in VALID_TRAY_TYPES):
            self.type = "Unknown"
        else:
            self.type = _type

        if(_map not in VALID_MAP_BLENDS):
            self.map = "Unknown"
        else:
            self.map = _map

        if(_spareTin):
            self.spareTin = True
        else:
            self.spareTin = False

    def setFrozen(self):
        self.frozen = True

    def setRefrigerated(self):
        self.frozen = False
     

class GarnishObject:

    def __init__(self,_portion_location,_portion_days,_allowable_types, _final_type,_contents = []):

        self.contents = []
        for item in enumerate(_contents):
            self.contents.append({"item": [], "portion_weight": 0.0})
            for field in item[1]:
                if field in self.contents[item[0]]:
                    self.contents[item[0]][field] = item[1][field]

        if(_portion_location not in VALID_FACILITIES):
            self.portion_location = "Unknown"
        else:
            self.portion_location = _portion_location

        self.portion_days = {}
        for cycle in VALID_CYCLES:
            self.portion_days.update({cycle: "Unknown"})
        for cycle in _portion_days:
            if(cycle not in VALID_CYCLES):
                print("Warning Cycle %s Not Recognized" % cycle)
            else:
                if(_portion_days[cycle] not in VALID_FACILITIES[self.portion_location]):
                    self.portion_days[cycle] =  "Unknown"
                else:
                    self.portion_days[cycle] =  _portion_days[cycle]

        self.allowable_types = []
        for item in _allowable_types:
            if(item in VALID_GARNISH_TYPES):
                self.allowable_types.append(item)

        self.final_type = _final_type

class FoodInProgressObject():

    def __init__(self, _name, _methods, _density = 1000, _transport_option = []):

        self.name = _name

        self.methods = []
        for method in _methods:
            if isinstance(method, FIPMethod):
                self.methods.append(method)
        else:
            raise ValueError("Food In Progress Object Created with Invalid Method")

        self.density = _density

        self.transport_options = []
        for item in _transport_options:
            if(item in VALID_TRANSPORT_CONTAINERS):
                self.transport_options.append(item)

class FIPMethod():

    def __init__(self, _prep_location, _prep_days, _pre_cook_max_batch_size, _pre_cook_min_batch_size, _batch_yield, _task_list, _rank, _contents = []):

        self.contents = []
        for item in enumerate(_contents):
            self.contents.append({"item": [], "percentage_by_weight": 0.0})
            for field in item[1]:
                if field in self.contents[item[0]]:
                    self.contents[item[0]][field] = item[1][field]

        if(_prep_location not in VALID_FACILITIES):
            self.prep_location = "Unknown"
        else:
            self.prep_location = _prep_location

        self.prep_days = {}
        for cycle in VALID_CYCLES:
            self.prep_days.update({cycle: "Unknown"})
        for cycle in _prep_days:
            if(cycle not in VALID_CYCLES):
                print("Warning Cycle %s Not Recognized" % cycle)
            else:
                if(_prep_days[cycle] not in VALID_FACILITIES[self.prep_location]):
                    self.prep_days[cycle] =  "Unknown"
                else:
                    self.prep_days[cycle] =  _prep_days[cycle]

        if(isinstance(_pre_cook_max_batch_size,float)):
            self.pre_cook_max_batch_size = _pre_cook_max_batch_size
        else:
            self.pre_cook_max_batch_size = 0.0

        if(isinstance(_pre_cook_min_batch_size,float)):
            self.pre_cook_min_batch_size = _pre_cook_min_batch_size
        else:
            self.pre_cook_min_batch_size = 0.0

        if(isinstance(_batch_yield,float)):
            self.batch_yield = _batch_yield
        else:
            self.batch_yield = 0.0

        self.task_list = []
        for task in _task_list:
            if(isinstance(task, PrepTaskObject)):
                self.task_list.append(task)
            else:
                print("WARNING: Ignoring Cooking Task for FIPMethod")

        if(isinstance(_rank,int)):
            self.rank = _rank
        else:
            self.rank = 99

class IngredientObject():

    def __init__(self, _name, _sku_options):

        self.name = _name

        self.sku_options = []
        for sku_option in _sku_options:
            if isinstance(sku_option, IngredientSKU):
                self.sku_options.append(sku_option)
        else:
            raise ValueError("IngredientObject Created with Invalid SKU Option")

class IngredientSKU():

    def __init__(self, _supplier, _supplier_SKU, _tovala_SKU = "", _lead_time = "", _container_size = "", _pack_size = 1, _lbs_per_container = -0.001, _cost_per_pack = 0, _type = "Unknown", _inventory = []):

        self.supplier = _supplier
        self.supplier_SKU = _supplier_SKU
        self.tovala_SKU = _tovala_SKU
        self.lead_time = _lead_time
        self.container_size = _container_size


        if(isinstance(_pack_size,int)):
            self.pack_size = _pack_size
        else:
            self.pack_size = 1
        
        if(isinstance(_lbs_per_container,float)):
            self.lbs_per_container = _lbs_per_container
        else:
            self.lbs_per_container = -0.001

        if(isinstance(_cost_per_pack,float)):
            self.cost_per_pack = _cost_per_pack
        else:
            self.cost_per_pack = 0

        if(_type in VALID_INVENTORY_TYPES):
            self.type = _type
        else:
            self.type = "Unknown"

        self.inventory = []
        for item in enumerate(_inventory):
            self.inventory.append({"quantity_pack": 0.0, "quantity_split": 0.0, "location": ""})
            for field in item[1]:
                if field in self.inventory[item[0]]:
                    self.inventory[item[0]][field] = item[1][field]

class PrepTaskObject():

    def __init__(self, _name, _task_notes, _process, _equipment_used):
        self.name = _name
        self.task_notes = _task_notes
        self.process = _process
        self.equipment_used = _equipment_used


def generateMealObjectsFromExcelSystem(filepath,version = 'v1'):
    wb = openpyxl.load_workbook(filename = filepath + 'Plating Guide Term 184.xlsx', data_only = True)

    if('Extended Plating Guide' not in wb.sheetnames):
        raise IndexError('Extended Plating Guide Not Found')

    mealMasterFileList = [f for f in listdir(filepath + 'Recipes Used/') if isfile(join(filepath + 'Recipes Used/', f))]

    meals = []

    for i in range(1,17):
        if(wb['Extended Plating Guide'][PLATING_GUIDE_COLUMNS_MAPPING[version]['MealCount']+str(5*i-3)].value>0):
            mealCount = wb['Extended Plating Guide'][PLATING_GUIDE_COLUMNS_MAPPING[version]['MealCount']+str(5*i-3)].value
            mealID = wb['Extended Plating Guide'][PLATING_GUIDE_COLUMNS_MAPPING[version]['MealCode']+str(5*i-3)].value
            [mealMasterFile] = difflib.get_close_matches(wb['Extended Plating Guide'][PLATING_GUIDE_COLUMNS_MAPPING[version]['MealName']+str(5*i-3)].value, mealMasterFileList, n = 1, cutoff = 0.1)

            mmF = openpyxl.load_workbook(filename = filepath + 'Recipes Used/' + mealMasterFile, data_only = True)

            #Compile Tray 1:
            mapColumn = wb['Extended Plating Guide'][PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_MAP']+str(5*i-3)].value
            film_type = "RTG"
            map_gas = "NO"
            tray_type = "TRAY"
            spareTin = False
            if("Yes" in mapColumn):
                map_gas = "YES"
            #if("No" in mapColumn):
            #    tray_type = "TRAY"
            #    map_gas = "NO"
            if(("Perf" in mapColumn)):
                film_type = "Perforated"
            if("VP" in mapColumn):
                tray_type = "VAC_PACK"
            if("CLM" in mapColumn):
                tray_type = "CLAMSHELL"
            if("BAG" in mapColumn):
                tray_type = "BAG"
            if(", T" in mapColumn):
                spareTin = True

            tray_1 = TrayObject(_portion_location = wb['Extended Plating Guide'][PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_PortionedAt']+str(5*i-3)].value,\
                                _portion_days = {'Sunday': wb['Extended Plating Guide'][PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_PortionDay']+str(5*i-3)].value},\
                                _film = film_type,\
                                _type = tray_type, \
                                _map = map_gas,\
                                _spareTin = spareTin,\
                                 _contents = [])

            for item in [wb['Extended Plating Guide'][PLATING_GUIDE_COLUMNS_MAPPING[version]['Tray1_Contents']+str(5*i-3+j)].value for j in range(5)]:
                if item == 0:
                    continue

                assembly_list = [mmF['ASSEMBLY']['B'+str(8+j)].value for j in range(15)]
                line = assembly_list.index(difflib.get_close_matches(item, [k for k in assembly_list if k], n = 1, cutoff = 0.5)[0])+8

                #---------
                #if ingredient, append Ingredient SKU + portion_weight
                #else, compile FoodInProgress object()

                print(item)
                print(line)
            raise ValueError


    



if __name__ == "__main__":

    VALID_CYCLES = ["Sunday"]
    VALID_FACILITIES.update({"Tubeway": ['Sunday','Monday','Thursday','Friday','Saturday']})
    VALID_FACILITIES.update({"Pershing": ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']})
    VALID_FACILITIES.update({"Unknown": ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']})
    VALID_FILM_TYPES = ['RTG', 'Boelter','Perforated']
    VALID_TRAY_TYPES = ['TRAY', 'CLAMSHELL','BAG','VAC_PACK']
    VALID_MAP_BLENDS = ['YES','NO']
    VALID_PROJECTIONS = ["1st_estimate", "friday_ordering_estimate", "sunday_ordering_estimate", "final"]
    VALID_TRANSPORT_CONTAINERS = ['55gal Drum']
    VALID_INVENTORY_TYPES = ['Frozen', 'Cooled', 'Dry-Goods']
    VALID_GARNISH_TYPES = ["Cup", "Sachet", "Bag", "Packet"]


    filepath = 'C:/Users/peter/Box Sync/Food Ops/Automated Recipe Building/Terms/Term 184/'
    

    generateMealObjectsFromExcelSystem(filepath)
    

    #print(VALID_FACILITIES)

    #tray = TrayObject([{"item": "None", "portion_weight": 2.0, "expected_portion_time": 0.0}],_portion_location = "Tubeway", _portion_days = {"Sunday": "Thursday","Monday": "Thursday","Tuesday": "Tuesday"}, _film = "RTG", _type = "TRAY", _map = "NO")
    #print(tray.contents[0]["item"])
    #print(tray.portion_location)
    #print(tray.portion_days)

    tray_block = '{"contents": [{"item": "None", "portion_weight": 2.0, "expected_portion_time": 0.0}], "count": {"projections": {"1st_estimate": {"Sunday": 10} },"final": {"Sunday": 1500.1} }, "portion_location": "Tubeway", "portion_day": {"Sunday": "Thursday","Monday": "Thursday","Tuesday": "Tuesday"},"film": "RTG", "type": "TRAY", "map": "NO", "frozen": ""}'

    tray1 = json.loads(tray_block)

    tray2 = TrayObject(_contents = tray1['contents'], _portion_location = tray1["portion_location"], _portion_days = tray1["portion_day"], _film = tray1["film"], _type = tray1["type"], _map = tray1["map"])
    #print(type(tray2))

    #print(tray2.contents[0]["item"])
    #print(tray2.portion_location)
    #print(tray2.portion_days)
    #print(tray1['count']['final'])

    meal = MealObject_Detailed(_id = 100, _count = MealCount(_projections = tray1['count']['projections'], _final = tray1['count']['final']), _meal = MealMaster(_name = "Test Meal",_trays = [tray2, tray2], _garnishes = []), _sleeving_location = "Tubeway", _sleeving_days = {"Sunday": "Saturday"})

    #print(meal.count.final['Sunday'])

    print(meal.meal.trays[0].portion_days[VALID_CYCLES[0]])
    print(meal.count.final[VALID_CYCLES[0]])