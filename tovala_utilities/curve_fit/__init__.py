# -*- coding: utf-8 -*-
"""
Created on Wed May 24 11:18:51 2017

@author: Peter
"""
#Python Imports
from __future__ import division
import csv
import numpy as np
import scipy.optimize as optimization
import scipy.signal as signalproc
import math

def expDecay(x,a,b):   #Fitting function for exponential decays. Is used by the function optimization.curve_fit
    return a*np.exp(-b*x)
   
def expDecayCurveFit(_time,_signal,x0): #Fit a single exponential decay to a portion of a trace between a local maxima and a local minima

    #x0=np.array([5,0.5])#,-1])  #Initial guess for the optimization.curve_fit function

    try:  #Attempt to fit the trace portion with the strongest weight on the middle of the decay, if cannot, return the initial guess.
        opt=optimization.curve_fit(expDecay,_time-_time[0],_signal,x0,1/np.power(np.abs(_signal-_signal[1*math.floor(len(_signal)/2)]),2))
        fitVar = opt[0]
        fitSig = fitVar[0]*np.exp(-fitVar[1]*(_time-_time[0]))-2#+fitVar[2]
    except RuntimeError:
        print("Could Not Find Fit for Decay Beginning at %2.2f" % _time[0])
        fitVar = x0
        fitSig = 0*(_time-_time[0])

    #print(fitVar)
    return [fitVar,fitSig]

def linFit(x,a,b):   #Fitting function for lines. Is used by the function optimization.curve_fit
    return a*x+b
   
def linCurveFit(_time,_signal,x0): #Fit a single linear function

    #x0=np.array([5,0.5])  #Initial guess for the optimization.curve_fit function

    try:  #Attempt to fit the trace portion with the strongest weight on the middle of the decay, if cannot, return the initial guess.
        opt=optimization.curve_fit(linFit,_time,_signal,x0)
        fitVar = opt[0]
        fitSig = fitVar[0]*np.exp(-fitVar[1]*(_time))-2#+fitVar[2]
    except RuntimeError:
        print("Could Not Find Fit for Decay Beginning at %2.2f" % _time[0])
        fitVar = x0
        fitSig = 0*(_time)

    #print(fitVar)
    return [fitVar,fitSig]

def sigmoidFit(x,x0,k,a,b):
    y = a / (1 + np.exp(-k*(x-x0))) + b
    return y

def sigmoidCurveFit(_time,_signal,guess):
    try:  #Attempt to fit the trace portion with the strongest weight on the middle of the decay, if cannot, return the initial guess.
        opt=optimization.curve_fit(sigmoidFit,_time,_signal,guess)
        fitVar = opt[0]
        fitSig = fitVar[2] / (1 + np.exp(-fitVar[1]*(_time-fitVar[0]))) + fitVar[3]
    except RuntimeError:
        print("Could Not Find Fit for Sigmoid Beginning at %2.2f" % _time[0])
        fitVar = guess
        fitSig = 0*(_time)

    #print(fitVar)
    return [fitVar,fitSig]

def chainDecay(x,a,b,c,decayConst):   #Fitting function for daughter of parent-daughter decay as in 2-isotope chain decay. Used by optimization.curve_fit
    return c*np.exp(-a*x)+b*np.exp(-decayConst*x)-2

def chainDecayCurveFit(_time,_signal,decayConst):  #Fit one chain decay function. Chain decay physics is how to treat parent-daughter isotope decay in nuclear physics

    x0=np.array([1.5,25,-8,decayConst])

    try: #Attempt to fit the trace portion with the strongest weight on the middle of the decay, if cannot, return the initial guess.
        opt=optimization.curve_fit(chainDecay,_time-_time[0],_signal,x0,np.sqrt(np.abs(0.1*_signal-_signal[math.floor(len(_signal)/2)])))
        fitVar = opt[0]
        _decayConst = fitVar[3]
        fitSig = fitVar[2]*np.exp(-fitVar[0]*(_time-_time[0]))+fitVar[1]*np.exp(-_decayConst*(_time-_time[0]))-2
    except RuntimeError:
        print("Could Not Find Fit for Chain Decay Beginning at %2.2f" % _time[0])
        fitVar = x0
        fitSig = 0*(_time-_time[0])

    #print(fitVar)
    return [fitVar,fitSig]

def expRise(x,a,b,c): #Fitting function for exponential rises (as in zero point thermal models for heating), Used by optimization.curve_fit
    return a - b*np.exp(-c*x)

def expRiseCurveFit(_time,_signal,x0): #Fit a single exponential rise to a trace

    try:  #Attempt to fit the trace portion with the strongest weight on the middle of the decay, if cannot, return the initial guess.
        opt=optimization.curve_fit(expRise,_time-_time[0],_signal,x0,1/np.power(np.abs(_signal-_signal[1*math.floor(len(_signal)/2)]),2))
        fitVar = opt[0]
        fitSig = fitVar[0]-fitVar[1]*np.exp(-fitVar[2]*(_time-_time[0]))
    except RuntimeError:
        print("Could Not Find Fit for Decay Beginning at %2.2f" % _time[0])
        fitVar = x0
        fitSig = 0*(_time-_time[0])

    #print(fitVar)
    return [fitVar,fitSig]
    
def driftingSinusoid(x,a,b,c,d,e): #Fitting function for oscillations that are drifting slowly in time
    return a + b*x + c*np.sin(d*x+e)

def driftingSinusoidCurveFit(_time,_signal,x0): #Fit a drifting sinusoid to a trace

    try:  #Attempt to fit the trace portion with the strongest weight on the average signal value, if cannot, return the initial guess.
        opt=optimization.curve_fit(driftingSinusoid,_time-_time[0],_signal,x0,1/np.power(np.abs(_signal-_signal[1*math.floor(len(_signal)/2)]),2))
        fitVar = opt[0]
        fitSig = fitVar[0]+fitVar[1]*(_time-_time[0])+fitVar[2]*np.sin(fitVar[3]*(_time-_time[0])+fitVar[4])
    except RuntimeError:
        print("Could Not Find Fit for Decay Beginning at %2.2f" % _time[0])
        fitVar = x0
        fitSig = 0*(_time-_time[0])

    #print(fitVar)
    return [fitVar,fitSig]

def cyclingExponential(x,tau,f,phi,minVal,maxVal,c1): #Fitting function for exponential with a duty cycle
    absMaxVal = (maxVal - minVal*np.exp(-c1*f*tau))/(1-np.exp(-c1*f*tau))
    c2 = -np.log(minVal/maxVal)/(tau*(1-f))
    conds1 = [((x%tau) >= (-phi))]
    conds2 = [((x%tau) < (f*tau-phi))]
    conds3 = [((x%tau) >= (-phi)+tau)]
    conds4 = [((x%tau) < ((1+f)*tau-phi))]
    conds5 = [((x%tau) >= (-phi)-tau)]
    conds6 = [((x%tau) < ((-1+f)*tau-phi))]
    
    conds = np.logical_or(np.logical_or(np.logical_and(conds1,conds2),np.logical_and(conds3,conds4)),np.logical_and(conds5,conds6))
    return np.piecewise(x,conds,[lambda x: (absMaxVal - (absMaxVal-minVal)*np.exp(-c1*((x+phi)%tau))), lambda x: (maxVal*np.exp(-c2*((x+phi-f*tau)%tau)))]) #[lambda x: maxVal, lambda x: minVal])

def cyclingExponentialCurveFit(_time,_signal,x0): #Fit a drifting sinusoid to a trace

    try:  #Attempt to fit the trace portion with the strongest weight on the average signal value, if cannot, return the initial guess.
        opt=optimization.curve_fit(cyclingExponential,_time-_time[0],_signal,x0,1/np.power(np.abs(_signal-_signal[1*math.floor(len(_signal)/2)]),2))
        fitVar = opt[0]
        fitSig = cyclingExponential(_time-_time[0],fitVar[0],fitVar[1],fitVar[2],fitVar[3],fitVar[4],fitVar[5])
    except RuntimeError:
        print("Could Not Find Fit for Decay Beginning at %2.2f" % _time[0])
        fitVar = x0
        fitSig = 0*(_time-_time[0])

    #print(fitVar)
    return [fitVar,fitSig]

def squareWave(x,tau,f,phi,minVal,maxVal): #Fitting function for square wave with a duty cycle
    conds1 = [((x%tau) >= (-phi))]
    conds2 = [((x%tau) < (f*tau-phi))]
    conds3 = [((x%tau) >= (-phi)+tau)]
    conds4 = [((x%tau) < ((1+f)*tau-phi))]
    conds5 = [((x%tau) >= (-phi)-tau)]
    conds6 = [((x%tau) < ((-1+f)*tau-phi))]
    
    conds = np.logical_or(np.logical_or(np.logical_and(conds1,conds2),np.logical_and(conds3,conds4)),np.logical_and(conds5,conds6))
    return np.piecewise(x,conds,[lambda x: maxVal, lambda x: minVal])

def squareWaveCurveFit(_time,_signal,x0): #Fit a drifting sinusoid to a trace

    try:  #Attempt to fit the trace portion with the strongest weight on the average signal value, if cannot, return the initial guess.
        opt=optimization.curve_fit(squareWave,_time-_time[0],_signal,x0,1/np.power(np.abs(_signal-_signal[1*math.floor(len(_signal)/2)]),2))
        fitVar = opt[0]
        fitSig = squareWave(_time-_time[0],fitVar[0],fitVar[1],fitVar[2],fitVar[3],fitVar[4])
    except RuntimeError:
        print("Could Not Find Fit for Decay Beginning at %2.2f" % _time[0])
        fitVar = x0
        fitSig = 0*(_time-_time[0])

    #print(fitVar)
    return [fitVar,fitSig]